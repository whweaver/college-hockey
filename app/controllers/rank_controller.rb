$LOAD_PATH.unshift(*Dir.glob('lib/lib/**/').map { |r| File.expand_path(r) })
require 'utils'

class RankController < ApplicationController

  def teams
    db_update do |db|
      @seasons = db.query("SELECT id FROM seasons ORDER BY id DESC;").map(&:first)
    end
    render 'rank/teams'
  end # teams

  def teams_data
    # Collect data from the database into a hash
    @data = {}
    db_update do |db|
      %i[sha unblocked accurate scored].each do |stat_type|
        if params[:type] == 'SOS'
          q = "SELECT teams.id, teams.name, logit(coeff.value, intercept.value, #{'1 - ' if params[:side] == 'D'}(btz.rating / (btz.rating + 100))) AS value
               FROM `#{stat_type}_btz` AS btz
               JOIN teams ON btz.id = #{'-' if params[:side] == 'D'}teams.id
               JOIN `#{stat_type}_btz_coeffs` AS coeff ON btz.grp = coeff.grp
               JOIN `#{stat_type}_btz_coeffs` AS intercept ON btz.grp = intercept.grp
               WHERE coeff.factor_name = 'vp_conversion'
                 AND coeff.coeff = 1
                 AND intercept.factor_name = 'vp_conversion'
                 AND intercept.coeff = 0
                 AND teams.season = #{params[:season]};"
        else
          team_table = params[:side] == 'D' ? 'tg2' : 'tg1'
          val_query = {
            sha: "AVG(#{team_table}.total_shot_attempts)",
            unblocked: "1 - (SUM(#{team_table}.total_blocked_shots) / SUM(#{team_table}.total_shot_attempts))",
            accurate: "SUM(#{team_table}.total_saved_shots + #{team_table}.goals) / SUM(#{team_table}.total_shot_attempts - #{team_table}.total_blocked_shots)",
            scored: "SUM(#{team_table}.goals) / SUM(#{team_table}.goals + #{team_table}.total_saved_shots)"
          }[stat_type]
          q = "SELECT *
               FROM (
                   SELECT t1.id, t1.name, #{val_query} AS value
                   FROM team_games AS tg1
                   JOIN teams AS t1 ON tg1.team_id = t1.id
                   JOIN team_games AS tg2 ON tg1.game_id = tg2.game_id AND tg1.team_id != tg2.team_id
                   JOIN teams AS t2 ON tg2.team_id = t2.id
                   WHERE t1.season = #{params[:season]}
                     AND t2.season = #{params[:season]}
                   GROUP BY t1.id, t1.name
               ) AS unfiltered
               WHERE value IS NOT NULL;"
        end
        db.query(q).each do |id, name, value|
          @data ||= {}
          @data[id] ||= {}
          @data[id][:name] = name
          @data[id][stat_type] = value
        end # query.each
      end # stat_type
    end # db_update

    # Render the resulting JSON
    render 'rank/teams_data'
  end # teams_data

end # RankController
