require 'lib/utils/setup_env'

Rake.application.rake_require 'utils'
rake_require 'data'
rake_require 'stats'

db_rule(/setup:\w*/) do |t, db|
  sql_name = t.name.gsub(':', '_')
  db.query(File.read(File.join('sql', sql_name).ext('sql')))
end
