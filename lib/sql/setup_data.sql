DROP DATABASE IF EXISTS hockey;;
CREATE DATABASE hockey
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_unicode_ci;;

USE hockey;;

SET FOREIGN_KEY_CHECKS = 0;;

CREATE TABLE schools (id       SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      chn_path VARCHAR(255)               UNIQUE);;

CREATE TABLE coaches (id             SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      first_name     VARCHAR(255)      NOT NULL,
                      last_name      VARCHAR(255)      NOT NULL,
                      alma_mater     VARCHAR(255),
                      grad_year      CHAR(8),
                      birthdate      DATE,
                      hometown_city  VARCHAR(255),
                      hometown_state VARCHAR(255),
                      chn_path       VARCHAR(255)               UNIQUE);;

CREATE TABLE arenas (id                SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                     name              VARCHAR(255)      NOT NULL,
                     city              VARCHAR(255),
                     state             VARCHAR(255),
                     year_built        CHAR(4),
                     year_closed       CHAR(4),
                     start_active_year CHAR(4),
                     end_active_year   CHAR(4),
                     capacity          MEDIUMINT UNSIGNED,
                     sheet_length      SMALLINT UNSIGNED,
                     sheet_width       SMALLINT UNSIGNED,
                     street_addr       VARCHAR(255),
                     chn_path          VARCHAR(255)      NOT NULL UNIQUE);;

CREATE TABLE players (id             MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      first_name     VARCHAR(255)       NOT NULL,
                      last_name      VARCHAR(255)       NOT NULL,
                      hometown_city  VARCHAR(255),
                      hometown_state VARCHAR(255),
                      birthdate      DATE,
                      chn_path       VARCHAR(255)                UNIQUE);;

CREATE TABLE seasons (id       CHAR(8)      NOT NULL PRIMARY KEY,
                      chn_path VARCHAR(255)          UNIQUE);;

CREATE TABLE referees (id         MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                       first_name VARCHAR(255),
                       last_name  VARCHAR(255),
                       CONSTRAINT uniq_name UNIQUE (first_name, last_name));;

CREATE TABLE teams (id                 SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    school_id          SMALLINT UNSIGNED NOT NULL,
                    name               VARCHAR(255)      NOT NULL,
                    season             CHAR(8)           NOT NULL,
                    arena_id           SMALLINT UNSIGNED,
                    nickname           VARCHAR(255),
                    conference         VARCHAR(255),
                    division           TINYINT  UNSIGNED,
                    wins               TINYINT  UNSIGNED,
                    losses             TINYINT  UNSIGNED,
                    ties               TINYINT  UNSIGNED,
                    reg_season_champs  BOOLEAN,
                    conf_tourn_champs  BOOLEAN,
                    ncaa_tourn         BOOLEAN,
                    chn_path_id        VARCHAR(255),
                    has_schedule       BOOLEAN           NOT NULL DEFAULT FALSE,
                    FOREIGN KEY (school_id) REFERENCES schools(id) ON DELETE CASCADE,
                    FOREIGN KEY (arena_id)  REFERENCES arenas (id) ON DELETE CASCADE,
                    CONSTRAINT uniq_team UNIQUE (school_id, season, arena_id));;

CREATE TABLE team_coaches (team_id  SMALLINT UNSIGNED NOT NULL,
                           coach_id SMALLINT UNSIGNED NOT NULL,
                           PRIMARY KEY (team_id, coach_id),
                           FOREIGN KEY (team_id ) REFERENCES teams  (id),
                           FOREIGN KEY (coach_id) REFERENCES coaches(id));;

CREATE TABLE rosters (team_id   SMALLINT  UNSIGNED NOT NULL,
                      player_id MEDIUMINT UNSIGNED NOT NULL,
                      num       TINYINT   UNSIGNED,
                      height    TINYINT   UNSIGNED,
                      weight    SMALLINT  UNSIGNED,
                      position  VARCHAR(5),
                      class     ENUM('Fr', 'So', 'Jr', 'Sr'),
                      captain   ENUM('C', 'A'),
                      PRIMARY KEY (team_id, player_id),
                      FOREIGN KEY (team_id)   REFERENCES teams  (id) ON DELETE CASCADE,
                      FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE);;

CREATE TABLE games (id               MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    team1_id         SMALLINT  UNSIGNED NOT NULL,
                    team2_id         SMALLINT  UNSIGNED NOT NULL,
                    date_time        DATETIME  NOT NULL,
                    time_zone        CHAR(2),
                    chn_box_path     VARCHAR(255),
                    chn_metrics_path VARCHAR(255),
                    type             VARCHAR(255),
                    sked_note        VARCHAR(255),
                    box_note         VARCHAR(255),
                    ot               TINYINT   UNSIGNED,
                    attendance       MEDIUMINT UNSIGNED,
                    arena_id         SMALLINT  UNSIGNED,
                    season           CHAR(8)   NOT NULL,
                    last_updated     TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    FOREIGN KEY (team1_id) REFERENCES teams (id) ON DELETE CASCADE,
                    FOREIGN KEY (team2_id) REFERENCES teams (id) ON DELETE CASCADE,
                    FOREIGN KEY (arena_id) REFERENCES arenas(id) ON DELETE CASCADE,
                    CONSTRAINT team1 UNIQUE (team1_id, date_time, time_zone),
                    CONSTRAINT team2 UNIQUE (team2_id, date_time, time_zone));;

CREATE TABLE team_games (team_id SMALLINT  UNSIGNED NOT NULL,
                         game_id MEDIUMINT UNSIGNED NOT NULL,
                         goals   TINYINT,
                         total_blocked_shots TINYINT UNSIGNED,
                         total_wide_shots    TINYINT UNSIGNED,
                         total_hit_posts     TINYINT UNSIGNED,
                         total_saved_shots   TINYINT UNSIGNED,
                         total_shot_attempts TINYINT UNSIGNED,
                         even_blocked_shots  TINYINT UNSIGNED,
                         even_wide_shots     TINYINT UNSIGNED,
                         even_hit_posts      TINYINT UNSIGNED,
                         even_saved_shots    TINYINT UNSIGNED,
                         even_shot_attempts  TINYINT UNSIGNED,
                         pp_blocked_shots    TINYINT UNSIGNED,
                         pp_wide_shots       TINYINT UNSIGNED,
                         pp_hit_posts        TINYINT UNSIGNED,
                         pp_saved_shots      TINYINT UNSIGNED,
                         pp_shot_attempts    TINYINT UNSIGNED,
                         close_blocked_shots TINYINT UNSIGNED,
                         close_wide_shots    TINYINT UNSIGNED,
                         close_hit_posts     TINYINT UNSIGNED,
                         close_saved_shots   TINYINT UNSIGNED,
                         close_shot_attempts TINYINT UNSIGNED,
                         blocks              TINYINT UNSIGNED,
                         fow                 TINYINT UNSIGNED,
                         fol                 TINYINT UNSIGNED,
                         PRIMARY KEY (team_id, game_id));;

CREATE TABLE team_periods (team_id SMALLINT  UNSIGNED NOT NULL,
                           game_id MEDIUMINT UNSIGNED NOT NULL,
                           period TINYINT    UNSIGNED NOT NULL,
                           shots  SMALLINT   UNSIGNED,
                           PRIMARY KEY (team_id, game_id, period));;

CREATE TABLE goals (id         MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    game_id    MEDIUMINT UNSIGNED NOT NULL,
                    period     TINYINT   UNSIGNED NOT NULL,
                    game_time  TIME               NOT NULL,
                    type       VARCHAR(15),
                    scorer_id  MEDIUMINT UNSIGNED,
                    assist1_id MEDIUMINT UNSIGNED,
                    assist2_id MEDIUMINT UNSIGNED,
                    FOREIGN KEY (game_id   ) REFERENCES games  (id) ON DELETE CASCADE,
                    FOREIGN KEY (scorer_id ) REFERENCES players(id) ON DELETE CASCADE,
                    FOREIGN KEY (assist1_id) REFERENCES players(id) ON DELETE CASCADE,
                    FOREIGN KEY (assist2_id) REFERENCES players(id) ON DELETE CASCADE,
                    CONSTRAINT unique_goal UNIQUE (game_id, period, game_time));;

CREATE TABLE penalties (id        MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        player_id MEDIUMINT UNSIGNED,
                        game_id   MEDIUMINT UNSIGNED NOT NULL,
                        period    TINYINT   UNSIGNED NOT NULL,
                        game_time TIME               NOT NULL,
                        duration  TIME               NOT NULL,
                        type      VARCHAR(255),
                        FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE,
                        FOREIGN KEY (game_id  ) REFERENCES games  (id) ON DELETE CASCADE);;


CREATE TABLE skater_games (player_id MEDIUMINT UNSIGNED NOT NULL,
                           game_id   MEDIUMINT UNSIGNED NOT NULL,
                           plusminus TINYINT   SIGNED,
                           fow       TINYINT   UNSIGNED,
                           fol       TINYINT   UNSIGNED,
                           blocks    TINYINT   UNSIGNED,
                           total_blocked_shots TINYINT UNSIGNED,
                           total_wide_shots    TINYINT UNSIGNED,
                           total_hit_posts     TINYINT UNSIGNED,
                           total_saved_shots   TINYINT UNSIGNED,
                           total_shot_attempts TINYINT UNSIGNED,
                           even_blocked_shots  TINYINT UNSIGNED,
                           even_wide_shots     TINYINT UNSIGNED,
                           even_hit_posts      TINYINT UNSIGNED,
                           even_saved_shots    TINYINT UNSIGNED,
                           even_shot_attempts  TINYINT UNSIGNED,
                           pp_blocked_shots    TINYINT UNSIGNED,
                           pp_wide_shots       TINYINT UNSIGNED,
                           pp_hit_posts        TINYINT UNSIGNED,
                           pp_saved_shots      TINYINT UNSIGNED,
                           pp_shot_attempts    TINYINT UNSIGNED,
                           close_blocked_shots TINYINT UNSIGNED,
                           close_wide_shots    TINYINT UNSIGNED,
                           close_hit_posts     TINYINT UNSIGNED,
                           close_saved_shots   TINYINT UNSIGNED,
                           close_shot_attempts TINYINT UNSIGNED,
                           PRIMARY KEY (player_id, game_id),
                           FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE,
                           FOREIGN KEY (game_id  ) REFERENCES games  (id) ON DELETE CASCADE);;

CREATE TABLE goalie_games (player_id   MEDIUMINT UNSIGNED NOT NULL,
                           game_id     MEDIUMINT UNSIGNED NOT NULL,
                           total_saves TINYINT   UNSIGNED,
                           total_goals TINYINT   UNSIGNED,
                           even_saves  TINYINT   UNSIGNED,
                           even_goals  TINYINT   UNSIGNED,
                           sh_saves    TINYINT   UNSIGNED,
                           sh_goals    TINYINT   UNSIGNED,
                           close_saves TINYINT   UNSIGNED,
                           close_goals TINYINT   UNSIGNED,
                           PRIMARY KEY (player_id, game_id),
                           FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE,
                           FOREIGN KEY (game_id  ) REFERENCES games  (id) ON DELETE CASCADE);;

CREATE TABLE referee_games (referee_id MEDIUMINT UNSIGNED NOT NULL,
                            game_id    MEDIUMINT UNSIGNED NOT NULL,
                            role       ENUM('ref', 'asst'),
                            PRIMARY KEY (referee_id, game_id),
                            FOREIGN KEY (referee_id) REFERENCES referees(id) ON DELETE CASCADE,
                            FOREIGN KEY (game_id   ) REFERENCES games   (id) ON DELETE CASCADE);;

CREATE TABLE player_aliases (chn_path   VARCHAR(255) NOT NULL,
                             first_name VARCHAR(255) NOT NULL,
                             last_name  VARCHAR(255) NOT NULL,
                             PRIMARY KEY (chn_path, first_name, last_name));;
INSERT INTO player_aliases (chn_path, first_name, last_name)
VALUES ('/players/career/CJ-Suess/29274', 'C.J.', 'Franklin');;

CREATE TABLE player_equivalents (chn_path1 VARCHAR(255) NOT NULL,
                                 chn_path2 VARCHAR(255) NOT NULL,
                                 PRIMARY KEY (chn_path1, chn_path2));;
INSERT INTO player_equivalents (chn_path1, chn_path2)
VALUES ('/players/career/Adam-Larkin/30260', '/players/career/Adam-Larkin/29889');;

SET FOREIGN_KEY_CHECKS = 1;;
