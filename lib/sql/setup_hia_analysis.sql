USE hockey;

DROP TABLE IF EXISTS time_zones;
CREATE TABLE time_zones (time_zone CHAR(2)        NOT NULL PRIMARY KEY,
                         offset    TINYINT SIGNED NOT NULL);
INSERT INTO time_zones (time_zone, offset)
VALUES ('ET', -5),
       ('CT', -6),
       ('MT', -7),
       ('PT', -8),
       ('AT', -9);

CREATE OR REPLACE VIEW conference_game_results AS
    SELECT games.id AS game_id, team1_id, team2_id, tg1.goals AS team1_goals, tg2.goals AS team2_goals, games.arena_id, games.type, games.season, games.attendance, games.time_zone
    FROM games
        JOIN team_games AS tg1 ON games.id = tg1.game_id AND games.team1_id = tg1.team_id
        JOIN team_games AS tg2 ON games.id = tg2.game_id AND games.team2_id = tg2.team_id
        JOIN teams ON tg1.team_id = teams.id
    WHERE games.last_updated > DATE_ADD(games.date_time, INTERVAL 6 HOUR)
        AND games.type = teams.conference
        AND games.type IS NOT NULL;

CREATE OR REPLACE VIEW home_wpct_over_time AS
    SELECT game_results.season, (SUM(team2_goals > team1_goals) + (SUM(team2_goals = team1_goals)/2)) / COUNT(*) AS winning_percentage, COUNT(*) AS num_games
    FROM game_results
    GROUP BY season;

CREATE OR REPLACE VIEW true_home_wpct_over_time AS
    SELECT game_results.season, (SUM(team2_goals > team1_goals) + (SUM(team2_goals = team1_goals)/2)) / COUNT(*) AS winning_percentage, COUNT(*) AS num_games
    FROM game_results
        JOIN teams ON game_results.team2_id = teams.id
    WHERE game_results.arena_id = teams.arena_id
    GROUP BY game_results.season;

CREATE OR REPLACE VIEW conference_home_wpct_over_time AS
    SELECT game_results.season, (SUM(team2_goals > team1_goals) + (SUM(team2_goals = team1_goals)/2)) / COUNT(*) AS winning_percentage, COUNT(*) AS num_games
    FROM game_results
        JOIN teams ON game_results.team1_id = teams.id
    WHERE game_results.type = teams.conference
    GROUP BY game_results.season;

CREATE OR REPLACE VIEW conference_true_home_wpct_over_time AS
    SELECT game_results.season, (SUM(team2_goals > team1_goals) + (SUM(team2_goals = team1_goals)/2)) / COUNT(*) AS winning_percentage, COUNT(*) AS num_games
    FROM game_results
        JOIN teams ON game_results.team2_id = teams.id AND game_results.type = teams.conference
    WHERE game_results.arena_id = teams.arena_id
    GROUP BY game_results.season;

CREATE OR REPLACE VIEW home_winning_percentage AS
    (
        SELECT 'All Games' AS type, SUM(winning_percentage * num_games) / SUM(num_games) AS winning_percentage, SUM(num_games) AS num_games
        FROM home_wpct_over_time
    )
    UNION
    (
        SELECT 'True Home Games' AS type, SUM(winning_percentage * num_games) / SUM(num_games) AS winning_percentage, SUM(num_games) AS num_games
        FROM true_home_wpct_over_time
    )
    UNION
    (
        SELECT 'Conference Games' AS type, SUM(winning_percentage * num_games) / SUM(num_games) AS winning_percentage, SUM(num_games) AS num_games
        FROM conference_home_wpct_over_time
    )
    UNION
    (
        SELECT 'Conference True Home Games' AS type, SUM(winning_percentage * num_games) / SUM(num_games) AS winning_percentage, SUM(num_games) AS num_games
        FROM conference_true_home_wpct_over_time
    );

CREATE OR REPLACE VIEW conference_home_wins_by_team AS
    SELECT team2_id AS team_id,
           SUM(team2_goals > team1_goals) AS wins,
           SUM(team2_goals < team1_goals) AS losses,
           SUM(team2_goals = team1_goals) AS ties,
           COUNT(*) AS games
    FROM conference_game_results
    GROUP BY team2_id;

CREATE OR REPLACE VIEW conference_away_wins_by_team AS
    SELECT team1_id AS team_id,
           SUM(team1_goals > team2_goals) AS wins,
           SUM(team1_goals < team2_goals) AS losses,
           SUM(team1_goals = team2_goals) AS ties,
           COUNT(*) AS games
    FROM conference_game_results
    GROUP BY team1_id;

CREATE OR REPLACE VIEW home_ice_advantage_by_team AS
    SELECT teams.id AS team_id,
           teams.season AS season,
           school_names.name AS team_name,
           ((home.losses + away.losses + home.ties / 2 + away.ties / 2) / (home.games + away.games)) / ((home.losses + home.ties / 2) / home.games) - 1 AS advantage,
           home.losses + home.ties / 2 AS home_losses,
           home.games AS home_games,
           (home.losses + away.losses + home.ties / 2 + away.ties / 2) AS total_losses,
           (home.games + away.games) AS total_games
    FROM teams
        JOIN conference_home_wins_by_team AS home ON teams.id = home.team_id
        JOIN conference_away_wins_by_team AS away ON teams.id = away.team_id
        JOIN schools ON teams.school_id = schools.id
        JOIN school_names ON schools.id = school_names.school_id;

CREATE OR REPLACE VIEW home_ice_advantage_by_school AS
    SELECT school_names.school_id AS school_id,
           school_names.name AS name,
           (SUM(total_losses) / SUM(total_games)) / (SUM(home_losses) / SUM(home_games)) - 1 AS advantage
    FROM home_ice_advantage_by_team
        JOIN teams ON home_ice_advantage_by_team.team_id = teams.id
        JOIN school_names ON teams.school_id = school_names.school_id
    GROUP BY school_names.school_id, school_names.name
    ORDER BY advantage DESC;

CREATE OR REPLACE VIEW home_ice_advantage_by_school_since_realignment AS
    SELECT school_names.school_id AS school_id,
           school_names.name AS name,
           (SUM(total_losses) / SUM(total_games)) / (SUM(home_losses) / SUM(home_games)) - 1 AS advantage
    FROM home_ice_advantage_by_team
        JOIN teams ON home_ice_advantage_by_team.team_id = teams.id
        JOIN school_names ON teams.school_id = school_names.school_id
    WHERE teams.season >= '20132014'
    GROUP BY school_names.school_id, school_names.name
    ORDER BY advantage DESC;

CREATE OR REPLACE VIEW home_ice_advantage_by_school_since_pearson AS
    SELECT school_names.school_id AS school_id,
           school_names.name AS name,
           (SUM(total_losses) / SUM(total_games)) / (SUM(home_losses) / SUM(home_games)) - 1 AS advantage
    FROM home_ice_advantage_by_team
        JOIN teams ON home_ice_advantage_by_team.team_id = teams.id
        JOIN school_names ON teams.school_id = school_names.school_id
    WHERE teams.season >= '20112012'
    GROUP BY school_names.school_id, school_names.name
    ORDER BY advantage DESC;

CREATE OR REPLACE VIEW home_ice_advantage_by_cur_school AS
    SELECT home_ice_advantage_by_school.*
    FROM home_ice_advantage_by_school
        JOIN schools ON home_ice_advantage_by_school.school_id = schools.id
    WHERE schools.chn_path IS NOT NULL
    ORDER BY advantage DESC;

CREATE OR REPLACE VIEW conference_home_wins_by_attendance AS
    SELECT attendance, IF(team2_goals > team1_goals, 1, IF(team2_goals = team1_goals, 0.5, 0)) AS home_win
    FROM conference_game_results AS cgr
        JOIN teams ON cgr.team2_id = teams.id AND cgr.type = teams.conference;

CREATE OR REPLACE VIEW conference_home_winning_margin_by_attendance AS
    SELECT attendance, capacity, attendance / capacity AS pct_filled, team2_goals - team1_goals AS home_margin
    FROM conference_game_results AS cgr
        JOIN teams ON cgr.team2_id = teams.id AND cgr.type = teams.conference
        JOIN arenas ON teams.arena_id = arenas.id AND cgr.arena_id = arenas.id
    WHERE attendance IS NOT NULL
        AND team1_goals IS NOT NULL
        AND team2_goals IS NOT NULL
        AND capacity > 0;

CREATE OR REPLACE VIEW hia_by_avg_attendance AS
    SELECT AVG(attendance) AS avg_att, capacity, AVG(attendance / capacity) AS pct_filled, advantage
    FROM home_ice_advantage_by_team AS hia
        JOIN conference_game_results AS cgr ON hia.team_id = cgr.team2_id
        JOIN teams ON hia.team_id = teams.id
        JOIN arenas ON teams.arena_id = arenas.id
    WHERE advantage IS NOT NULL
        AND attendance IS NOT NULL
    GROUP BY team_id;

CREATE OR REPLACE VIEW new_teams AS
    SELECT *
    FROM regular_teams AS cur_rt
    WHERE NOT EXISTS
    (
        SELECT *
        FROM regular_teams AS prev_rt
        WHERE cur_rt.school_id = prev_rt.school_id
            AND cur_rt.season - 10001 = prev_rt.season
    )
    ORDER BY season;

CREATE OR REPLACE VIEW old_teams AS
    SELECT *
    FROM regular_teams AS cur_rt
    WHERE NOT EXISTS
    (
        SELECT *
        FROM regular_teams AS next_rt
        WHERE cur_rt.school_id = next_rt.school_id
            AND cur_rt.season + 10001 = next_rt.season
    )
    ORDER BY season;

CREATE OR REPLACE VIEW new_conferences AS
    SELECT cur_team.id, cur_team.name, cur_team.season
    FROM teams AS cur_team
        JOIN teams AS prev_team ON cur_team.school_id = prev_team.school_id
    WHERE cur_team.conference != prev_team.conference
        AND cur_team.season - 10001 = prev_team.season
    ORDER BY cur_team.season;

CREATE OR REPLACE VIEW team_changes AS
    SELECT seasons.id, COUNT(new_teams.school_id) + COUNT(old_teams.school_id) + COUNT(new_conferences.id) AS ct
    FROM seasons
        LEFT JOIN new_teams ON seasons.id = new_teams.season
        LEFT JOIN old_teams ON seasons.id = old_teams.season
        LEFT JOIN new_conferences ON seasons.id = new_conferences.season
    GROUP BY seasons.id;

CREATE OR REPLACE VIEW games_in_time_zones AS
    SELECT arena_id, time_zone, COUNT(*) AS games
    FROM games
    WHERE arena_id IS NOT NULL
        AND time_zone IS NOT NULL
    GROUP BY arena_id, time_zone;

CREATE OR REPLACE VIEW arena_time_zones AS
    SELECT games_in_time_zones.arena_id, games_in_time_zones.time_zone
    FROM games_in_time_zones
    JOIN (
        SELECT arena_id, MAX(games) AS games
        FROM games_in_time_zones
        GROUP BY arena_id
    ) AS max_games ON games_in_time_zones.arena_id = max_games.arena_id
    WHERE games_in_time_zones.games = max_games.games;

CREATE OR REPLACE VIEW time_zone_travel_distance AS
    SELECT game_id,
           tz_game.offset - tz_away.offset AS tz_diff
    FROM conference_game_results AS cgr
        JOIN teams AS away ON cgr.team1_id = away.id
        JOIN arena_time_zones AS atz ON away.arena_id = atz.arena_id
        JOIN time_zones AS tz_game ON cgr.time_zone = tz_game.time_zone
        JOIN time_zones AS tz_away ON atz.time_zone = tz_away.time_zone;

CREATE OR REPLACE VIEW home_winning_percentage_by_time_zone_diff AS
    SELECT tztd.tz_diff,
           (SUM(cgr.team2_goals > cgr.team1_goals) + SUM(cgr.team2_goals = cgr.team1_goals)/2)/COUNT(*) AS wpct,
           COUNT(*) AS games
    FROM conference_game_results AS cgr
        JOIN time_zone_travel_distance AS tztd ON cgr.game_id = tztd.game_id
    GROUP BY tztd.tz_diff;