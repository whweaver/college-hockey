USE hockey;;

CREATE OR REPLACE VIEW home_results_by_team AS
    SELECT team2_id AS team_id, SUM(team2_goals > team1_goals) AS wins, SUM(team2_goals = team1_goals) AS ties, SUM(team2_goals < team1_goals) AS losses, COUNT(*) AS games
    FROM game_results
    GROUP BY team2_id;;

CREATE OR REPLACE VIEW away_results_by_team AS
    SELECT team1_id AS team_id, SUM(team1_goals > team2_goals) AS wins, SUM(team1_goals = team2_goals) AS ties, SUM(team1_goals < team2_goals) AS losses, COUNT(*) AS games
    FROM game_results
    GROUP BY team1_id;;

CREATE OR REPLACE VIEW home_opponents AS
    SELECT tg2.team_id AS team_id, tg2.game_id AS game_id, tg1.team_id AS other_team_id
    FROM games
        JOIN team_games AS tg1 ON games.id = tg1.game_id AND games.team1_id = tg1.team_id
        JOIN team_games AS tg2 ON games.id = tg2.game_id AND games.team2_id = tg2.team_id;;

CREATE OR REPLACE VIEW away_opponents AS
    SELECT tg1.team_id AS team_id, tg1.game_id AS game_id, tg2.team_id AS other_team_id
    FROM games
        JOIN team_games AS tg1 ON games.id = tg1.game_id AND games.team1_id = tg1.team_id
        JOIN team_games AS tg2 ON games.id = tg2.game_id AND games.team2_id = tg2.team_id;;

CREATE OR REPLACE VIEW opponents AS
    (
        SELECT *
        FROM home_opponents
    )
    UNION
    (
        SELECT *
        FROM away_opponents
    );;

CREATE OR REPLACE VIEW winning_percentage AS
    SELECT home.team_id AS team_id,
           (0.8 * (home.wins + home.ties / 2) + 1.2 * (away.wins + away.ties / 2)) / (0.8 * home.wins + 1.2 * home.losses + home.ties + 1.2 * away.wins + 0.8 * away.losses + away.ties) AS weighted_wp,
           (home.wins + home.ties / 2 + away.wins + away.ties / 2) / (home.games + away.games) AS wp
    FROM home_results_by_team AS home
        JOIN away_results_by_team AS away ON home.team_id = away.team_id;;

CREATE OR REPLACE VIEW opponents_winning_percentage AS
    SELECT opponents.team_id AS team_id, SUM(winning_percentage.weighted_wp) / COUNT(*) AS wp
    FROM opponents
        JOIN winning_percentage ON opponents.other_team_id = winning_percentage.team_id
    GROUP BY opponents.team_id;;

CREATE OR REPLACE VIEW opponents_opponents_winning_percentage AS
    SELECT opponents.team_id AS team_id, SUM(winning_percentage.weighted_wp) / COUNT(*) AS wp
    FROM opponents
        JOIN opponents AS opponents_opponents ON opponents.other_team_id = opponents_opponents.team_id
        JOIN winning_percentage ON opponents_opponents.other_team_id = winning_percentage.team_id
    GROUP BY opponents.team_id;;

CREATE OR REPLACE VIEW rpi AS
    SELECT winning_percentage.team_id AS team_id,
           0.25 * winning_percentage.wp + 0.21 * opponents_winning_percentage.wp + 0.54 * opponents_opponents_winning_percentage.wp AS rpi
    FROM winning_percentage
        JOIN opponents_winning_percentage ON winning_percentage.team_id = opponents_winning_percentage.team_id
        JOIN opponents_opponents_winning_percentage ON winning_percentage.team_id = opponents_opponents_winning_percentage.team_id;;

CREATE OR REPLACE VIEW ranked_rpi AS
    SELECT rankee_team.id AS team_id,
           (
                SELECT COUNT(1)
                FROM teams AS above_team
                    JOIN rpi AS above_rpi
                WHERE above_rpi.rpi > rankee_rpi.rpi
                    AND above_team.season = rankee_team.season
           ) + 1 AS rank
    FROM teams AS rankee_team
        JOIN rpi AS rankee_rpi ON rankee_team.id = rankee_rpi.team_id;;

CREATE OR REPLACE VIEW qwb AS
    SELECT NULL;;

CREATE OR REPLACE VIEW rpi_with_qwb AS
    SELECT NULL;;

CREATE OR REPLACE VIEW pwr AS
    SELECT NULL;;
