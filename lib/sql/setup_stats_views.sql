USE hockey;;

CREATE OR REPLACE VIEW game_results AS
    SELECT games.id AS game_id,
           team1_id,
           team2_id,
           tg1.goals AS team1_goals,
           tg2.goals AS team2_goals,
           games.arena_id AS arena_id,
           type,
           games.season AS season,
           attendance,
           IF(capacity = 0 OR capacity IS NULL, NULL, attendance / capacity) AS attendance_pct,
           tz_travel.tz_diff AS tz_travel,
           IF(games.arena_id = t2.arena_id, 1, 0) AS home
    FROM games
    JOIN team_games AS tg1 ON games.id = tg1.game_id AND games.team1_id = tg1.team_id
    JOIN team_games AS tg2 ON games.id = tg2.game_id AND games.team2_id = tg2.team_id
    LEFT JOIN arenas ON games.arena_id = arenas.id
    LEFT JOIN time_zone_travel_distance AS tz_travel ON games.id = tz_travel.game_id
    JOIN teams AS t2 ON games.team2_id = t2.id
    WHERE games.last_updated > DATE_ADD(games.date_time, INTERVAL 6 HOUR)
        AND games.type != 'Exhibition';;

CREATE OR REPLACE VIEW team_games_combined AS
    (
        SELECT team1_id AS team_id,
               team1_goals AS team_goals,
               game_id,
               team2_id AS other_team_id,
               team2_goals AS other_goals,
               team2_id AS home_team,
               arena_id,
               attendance_pct,
               tz_travel,
               -home AS home
        FROM game_results
    )
    UNION
    (
        SELECT team2_id AS team_id,
               team2_goals AS team_goals,
               game_id,
               team1_id AS other_team_id,
               team1_goals AS other_goals,
               team2_id AS home_team,
               arena_id,
               attendance_pct,
               tz_travel,
               home
        FROM game_results
    );;

CREATE OR REPLACE VIEW conferences AS
    SELECT DISTINCT(conference) AS conference
    FROM teams;;

CREATE OR REPLACE VIEW school_last_season AS
    SELECT school_id, MAX(season) AS season
    FROM teams
    GROUP BY school_id;;

CREATE OR REPLACE VIEW school_names AS
    SELECT teams.school_id AS school_id, name
    FROM teams
        JOIN school_last_season ON teams.school_id = school_last_season.school_id
    WHERE school_last_season.season = teams.season;;

CREATE OR REPLACE VIEW num_games AS
    SELECT team_id, COUNT(*) AS games
    FROM team_games
    GROUP BY team_id;;

CREATE OR REPLACE VIEW most_games AS
    SELECT season, MAX(games) AS games
    FROM num_games
        JOIN teams ON num_games.team_id = teams.id
    GROUP BY season;;

CREATE OR REPLACE VIEW regular_teams AS
    SELECT team_id, school_id, teams.season
    FROM num_games
        JOIN teams ON num_games.team_id = teams.id
        JOIN most_games ON teams.season = most_games.season
    WHERE num_games.games > most_games.games / 2;;

CREATE OR REPLACE VIEW regular_games AS
    SELECT tgc.team_id, tgc.team_goals, tgc.game_id, tgc.other_team_id, tgc.other_goals, rt1.season
    FROM team_games_combined AS tgc
        JOIN regular_teams AS rt1 ON tgc.team_id = rt1.team_id
        JOIN regular_teams AS rt2 ON tgc.other_team_id = rt2.team_id;;
