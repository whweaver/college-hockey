"""BTZ victory point coefficients calculator"""

import argparse
import pandas
from sklearn.linear_model import LogisticRegression
import utils
import btz_calculator
import yaml
import multiprocessing
import numpy
import time
import os

class BtzVpCoeffsCalculator(btz_calculator.BtzCalculator):

    def calc_coeffs(self, grp, data):
        if(len(data) > 1):
            metric, victory = self.pivot_array(self.clean_db_data(data))
            if len(numpy.unique(victory)) > 1:
                regression = LogisticRegression(solver='saga', fit_intercept=True, n_jobs=-1, max_iter=10_000)
                regression.fit(pandas.DataFrame(metric), pandas.DataFrame(victory).squeeze())
                return [grp, regression.coef_[0][0], regression.intercept_[0]]
            else:
                return [grp, 0, 0]
        else:
            return [grp, 0, 0]

    def __create_metric_temp_table(self, c):
        metric_query = os.popen(f'ruby lib/lib/utils/erb_from_cfg.rb btz/full_metric_query.sql.erb {self.cfg_filename}').read()
        try:
            c.execute(f"""CREATE TEMPORARY TABLE metric_table AS
                          SELECT grp, metric, victory
                          FROM ({metric_query}) AS mt
                          JOIN `{self.cfg['ratings_table']}` AS rt ON mt.id_1 = rt.id
                          WHERE metric IS NOT NULL
                            AND victory IS NOT NULL
                            AND id_1 > 0;""")
        except:
            print("Failed SQL query was:")
            print(metric_query)
            raise

    ###########################################################################
    # Public API Methods
    ###########################################################################

    def update_coeffs(self):
        """Update the coefficients in the database."""

        with open('lib/yml/db.yml', 'r') as yml:
            db_cfg = utils.desymbolize_keys(yaml.safe_load(yml))

        with utils.DBConnection(db_cfg) as c:
            self.__create_metric_temp_table(c)
            with utils.Pool(multiprocessing.cpu_count()) as pool:
                results = []
                for grp in self.groups(c):
                    c.execute(f"""SELECT metric, victory FROM metric_table WHERE grp = {grp};""")
                    results.append(pool.apply_async(self.calc_coeffs, args=(grp, c.fetchall())))
                for result in results:
                    grp, coef, intercept = result.get()
                    c.execute(f"""UPDATE {self.cfg['coeffs_table']} SET value = {coef} WHERE grp = {grp} AND factor_name = 'vp_conversion' AND coeff = 1;""")
                    c.execute(f"""UPDATE {self.cfg['coeffs_table']} SET value = {intercept} WHERE grp = {grp} AND factor_name = 'vp_conversion' AND coeff = 0;""")

def parse_args():
    """Parse arguments to the program.
    
    Returns the arguments passed to the program.
    """
    p = argparse.ArgumentParser(description='Calculate BTZ ratings.')
    p.add_argument('cfg_file', metavar='C', type=str, nargs=1, help='The YAML configuration file for the BTZ system.')
    return p.parse_args()

def main():
    """Main function"""

    # Get the input to the program
    args = parse_args()

    # Calculate the VP coeffs and put them in the database
    bvcc = BtzVpCoeffsCalculator(args.cfg_file[0], 0)
    bvcc.update_coeffs()

if __name__ == '__main__':
    main()
