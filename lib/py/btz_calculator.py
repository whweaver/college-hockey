import numpy
from decimal import *
import os
import utils

class BtzCalculator:

    ###########################################################################
    # Special Methods
    ###########################################################################

    def __init__(self, cfg_filename, group):
        """Initializer
        
        Positional arguments:
        cfg -- Configuration dict.
        group -- Group to calculate ratings for.
        """

        # Save configuration
        self.cfg_filename = cfg_filename
        self.cfg = utils.desymbolize_keys(utils.get_configuration(cfg_filename))
        self.group = group

    ###########################################################################
    # Private Helper Methods
    ###########################################################################

    def clean_db_value(self, value):
        """Cleans a database value so it's useful in python.

        Positional arguments:
        value -- The value to clean.

        Returns a clean version of the value.
        """
        if isinstance(value, Decimal):
            return float(value)
        else:
            return value

    def clean_db_data(self, results):
        """Cleans database data to be useful in python.

        Positional arguments:
        results -- The raw database results.

        Returns a clean version of the results
        """
        return [[self.clean_db_value(value) for value in row] for row in results]

    def pivot_array(self, array):
        """Reorganize an array that's organized of a list of rows in to a list of columns.
        
        Positional arguments:
        array -- The array to reorganize.

        Returns the reorganized array.
        """
        columns = [[] for i in range(len(array[0]))]
        for row in array:
            for value, col in zip(row, columns):
                col.append(value)
        return [numpy.array(col) for col in columns]

    def create_vp_temp_table(self, c):
        vp_query = os.popen(f'ruby lib/lib/utils/erb_from_cfg.rb btz/select_vp.sql.erb {self.cfg_filename}').read()
        try:
            c.execute(f"""CREATE TEMPORARY TABLE vp_table AS
                          SELECT *
                          FROM ({vp_query}) AS vp
                          JOIN `{self.cfg['ratings_table']}` AS rt ON rt.id = vp.id_1;""")
        except:
            print("Failed SQL query was:")
            print(vp_query)
            raise

    def retrieve_vp_table(self, c, grp):
        """Get the VP table from the database.

        Positional arguments:
        c -- The database cursor.

        Each column becomes a numpy array as an attribute of the object.
        """
        if 'adjustments' in self.cfg and self.cfg['adjustments'] is not None:
            self.adj_factor_names = tuple(self.cfg['adjustments'].keys())
            adj_factor_string = f", {', '.join(self.adj_factor_names)}"
        else:
            self.adj_factor_names = tuple()
            adj_factor_string = ""
        c.execute(f"""SELECT id_1, id_2, metric, vp {adj_factor_string}
                      FROM vp_table
                      WHERE grp = {grp};""")
        return c.fetchall()

    def parse_vp_table(self, data):
        id_1, id_2, metric, vp, *adj_factors = self.pivot_array(self.clean_db_data(data))
        adj_factor_dict = {}
        for name, factor_array in zip(self.adj_factor_names, adj_factors):
            adj_factor_dict[name] = factor_array
        return {
            'id_1': id_1,
            'id_2': id_2,
            'metric': metric,
            'vp': vp,
            'adj_factors': adj_factor_dict
        }

    def groups(self, c):
        c.execute(f"""SELECT DISTINCT grp FROM `{self.cfg['ratings_table']}`;""")
        return (record[0] for record in c.fetchall())
