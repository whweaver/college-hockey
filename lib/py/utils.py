"""Utilities"""

import MySQLdb
import yaml
import multiprocessing

class DBConnection:
    """Context manager for a database connection"""

    def __init__(self, db_cfg):
      """Initializer
      Positional arguments:
      db_cfg -- The database configuration.
      """
      self.db_cfg = db_cfg

    def __enter__(self):
        """Enter the context.
        Returns a cursor.
        """
        self.db = MySQLdb.connect(host=self.db_cfg['hostname'],
                                  user=self.db_cfg['username'],
                                  passwd=self.db_cfg['password'],
                                  db=self.db_cfg['database'],
                                  port=self.db_cfg['port'],
                                  unix_socket=self.db_cfg['socket'])
        self.cursor = self.db.cursor()
        return self.cursor

    def __exit__(self, type, value, traceback):
        """Exit the context.
        Positional arguments:
        type -- The exception type.
        value -- The exception value.
        traceback -- The exception traceback.
        """
        if type is None:
            self.db.commit()
        else:
            self.db.rollback()
        self.cursor.close()
        self.db.close()

class Pool:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        self.pool = multiprocessing.Pool(*self.args, **self.kwargs)
        return self.pool

    def __exit__(self, type, value, traceback):
        self.pool.close()
        self.pool.join()

def desymbolize_keys(o):
    """Removes the : ruby symbol notation from dictionary keys.
    
    Positional arguments:
    o -- The object to desymbolize.

    Returns a new object that has the keys desymbolized.
    """
    if isinstance(o, dict):
        new_o = {}
        for k in o:
            new_k = desymbolize_keys(k)
            new_v = desymbolize_keys(o[k])
            new_o[new_k] = new_v
        return new_o

    elif isinstance(o, list):
        return (desymbolize_keys(e) for e in o)

    elif isinstance(o, str):
        if o[0] == ':':
            return o[1:]
        else:
            return o

    else:
        return o

def get_configuration(filepath):
    """Gets the configuration.
    
    Positional arguments:
    filepath -- Path to the configuration file.
    """
    cfg = None
    with open(filepath, 'r') as cfg_yml:
        cfg = yaml.safe_load(cfg_yml)

    return cfg
