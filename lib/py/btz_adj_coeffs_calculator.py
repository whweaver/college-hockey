import argparse
import numpy
from sklearn import linear_model
import pandas
import utils
import btz_calculator
import yaml
import multiprocessing

class BtzAdjCoeffsCalculator(btz_calculator.BtzCalculator):

    ###########################################################################
    # Private Helper Methods
    ###########################################################################

    def __data_to_avg_histogram(self, bin_center, bin_size, x, y):
        """Generate a histogram from specified data.
        Positional arguments:
        bin_center -- The center of any bin.
        bin_size -- The size of the bins.
        x -- The independent variable.
        y -- The dependent variable.

        Returns the x bins and their corresponding average ys.
        """

        # Find the center of the bin for each x value
        bin_left = bin_center - (bin_size / 2)
        offsets_from_bin_left = x - bin_left
        bin_indices = numpy.floor(offsets_from_bin_left / bin_size)
        bin_lefts = bin_indices * bin_size + bin_left
        bin_centers = bin_lefts + (bin_size / 2)

        # Average all the y, grouped by the x bins
        bins = numpy.unique(bin_centers)
        avg_y = numpy.array([])
        for bin_center in bins:
            avg_y = numpy.append(avg_y, y[bin_centers == bin_center].mean())

        return bins, avg_y

    def __linear_regression(self, x, y):
        """Computes a linear regression for the given adjustment.

        Positional arguments:
        x -- The independent variable.
        y -- The dependent variable.

        Returns a dictionary of the coefficients with their ID.
        """
        reg = linear_model.LinearRegression(fit_intercept=True, normalize=False, copy_X=True, n_jobs=-1).fit(pandas.DataFrame(x), pandas.DataFrame(y))
        return {0: reg.intercept_[0], 1: reg.coef_[0][0]}

    def __calc_factor_coeffs(self, id_1, adj_name, adj_factors, vp):
        """Calculate the rating coefficients for the given factor.
        Positional arguments:
        adj_name -- The name of the adjustment factor to calculate.
        """

        # Logit vp (assume victory points follow a logistic curve)
        logit_vp = numpy.log(vp / (1 - vp))
        
        # Convert data into histogram of average victory points for each bin.
        adj_cfg = self.cfg['adjustments'][adj_name]
        bins, avg_vps = self.__data_to_avg_histogram(adj_cfg['bin_center'], adj_cfg['bin_size'], adj_factors[adj_name], logit_vp)

        # Run regression algorithm on the binned data
        return {
            'linear': self.__linear_regression
        }[self.cfg['adjustments'][adj_name]['regression_type']](bins, avg_vps)

    ###########################################################################
    # Public API Methods
    ###########################################################################

    def calc_coeffs(self, grp, data):
        parsed_data = self.parse_vp_table(data)
        adj_coeffs = {}
        if 'adjustments' in self.cfg and self.cfg['adjustments'] is not None:
            for adj_name in self.cfg['adjustments']:
                adj_coeffs[adj_name] = self.__calc_factor_coeffs(parsed_data['id_1'], adj_name, parsed_data['adj_factors'], parsed_data['vp'])
        return [grp, adj_coeffs]

    def update_coeffs(self):
        """Retrieve the data from the database."""
        
        # Get database connection information
        with open('lib/yml/db.yml', 'r') as yml:
            db_cfg = utils.desymbolize_keys(yaml.safe_load(yml))

        # Get relevant information from database
        with utils.DBConnection(db_cfg) as c:
            self.create_vp_temp_table(c)
            with utils.Pool(multiprocessing.cpu_count()) as pool:
                results = []
                for grp in self.groups(c):
                    results.append(pool.apply_async(self.calc_coeffs, args=(grp, self.retrieve_vp_table(c, grp))))
                for result in results:
                    grp, adj_coeffs = result.get()
                    for name in adj_coeffs:
                        for coeff in adj_coeffs[name]:
                            c.execute(f"""UPDATE `{self.cfg['coeffs_table']}`
                                          SET value = {adj_coeffs[name][coeff]}
                                          WHERE grp = {grp} AND
                                                factor_name = '{name}' AND
                                                coeff = {coeff};""")

def parse_args():
    """Parse arguments to the program.
    
    Returns the arguments passed to the program.
    """
    p = argparse.ArgumentParser(description='Calculate BTZ adjustment coefficients.')
    p.add_argument('cfg_file', metavar='C', type=str, nargs=1, help='The YAML configuration file for the BTZ system.')
    return p.parse_args()

def main():
    """Calculate BTZ coefficients."""

    # Get the input to the program
    args = parse_args()

    # Calculate the coeffs and put them in the database
    brc = BtzAdjCoeffsCalculator(args.cfg_file[0], 0)
    brc.update_coeffs()

# Run the program
if __name__ == '__main__':
    main()
