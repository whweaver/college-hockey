import argparse
from scipy.stats.mstats import gmean
import numpy
import utils
import btz_calculator
import yaml
import multiprocessing

class BtzRatingsCalculator(btz_calculator.BtzCalculator):

    ###########################################################################
    # Private Helper Methods
    ###########################################################################

    def __linear_adj_factors(self, factor_name):
        """Get the adjustment factor for each comparison for a linear factor.

        Positional arguments:
        factor_name -- The name of the factor to compute.

        Returns a numpy array of adjustment factors for each comparison.
        """
        if(self.adj_coeffs[factor_name][1] is None or self.adj_coeffs[factor_name][0] is None):
            return 0
        else:
            return self.adj_coeffs[factor_name][1] * self.adj_factors[factor_name] + self.adj_coeffs[factor_name][0]

    def __adj_factors(self, factor_name):
        """Get the adjustment factor for each comparison for a single factor.

        Positional arguments:
        factor_name -- The name of the factor to compute.

        Returns a numpy array of adjustment factors for each comparison.
        """

        # Convert adjustment data to expected logit VP
        exp_logit_vp = {
            'linear': self.__linear_adj_factors
        }[self.cfg['adjustments'][factor_name]['regression_type']](factor_name)

        # Convert expected logit VP to rating coefficient
        # To convert expected logit VP to VP, the logistic function is taken: V = 1 / (1 + exp(-L))
        # To convert expected VP to a rating coefficient, the following function is used: C = V / (1 - V)
        # When these functions are nested, they simplify to just C = exp(L)
        return numpy.exp(exp_logit_vp)

    def __calc_combined_adj_factors(self):
        """Get the adjustments factors for each comparison."""
        result = 1.0
        for factor_name in self.adj_factors:
            result *= self.__adj_factors(factor_name)
        return result

    def __calc_predicted_vp(self, adj_factors):
        """Get the predicted victory points for each comparison.
        
        Positional arguments:
        adj_factors -- The combined adjustment factor for each comparison.

        Returns a numpy array of the predicted victory points for each comparison.
        """
        r1_adjd = self.r1 * adj_factors
        r2_adjd = self.r2
        return r1_adjd / (r1_adjd + r2_adjd)

    def __rating_array_from_ids(self, ids, ratings):
        """Get a numpy array of ratings corresponding to a list of IDs.

        Positional arguments:
        ids -- The list of IDs.
        ratings -- The dictionary of ratings by ID.

        Returns a numpy array of ratings.
        """
        return numpy.array([ratings[id] for id in ids])

    def __sum_vp_by_id(self, id_order, ids, vp):
        """Sum victory points grouped by ID.

        Positional arguments:
        id_order -- Numpy array of unique ids in the order their vp should be returned.
        id -- Numpy array of ids.
        vp -- Numpy array of victory points.

        Returns a numpy array of victory points."""
        a = numpy.array([vp[ids == id].sum() for id in id_order])
        return a

    def __set_ratings(self, ratings):
        """Set a new set of ratings.

        Positional arguments:
        ratings -- The new ratings in numpy form.
        """
        self.rating = ratings
        ratings_by_id = {}
        for id, rating in zip(self.id, self.rating):
            ratings_by_id[id] = rating
        self.r1 = self.__rating_array_from_ids(self.id_1, ratings_by_id)
        self.r2 = self.__rating_array_from_ids(self.id_2, ratings_by_id)

    ###########################################################################
    # Public API Methods
    ###########################################################################

    def calc_grp_ratings(self, grp, vp_data, seed_ratings_data, adj_coeff_data):
        # Get VP data
        parsed_vp = self.parse_vp_table(vp_data)
        self.id_1 = parsed_vp['id_1']
        self.id_2 = parsed_vp['id_2']
        self.vp = parsed_vp['vp']
        self.adj_factors = parsed_vp['adj_factors']

        # Get initial ratings
        all_results = self.clean_db_data(seed_ratings_data)
        ratings_by_id = {}
        self.id, self.rating = self.pivot_array(all_results)
        for id, rating in all_results:
            ratings_by_id[id] = rating

        # Get adjustment coefficients
        self.adj_coeffs = {}
        for name, coeff, value in self.clean_db_data(adj_coeff_data):
            self.adj_coeffs.setdefault(name, {})[coeff] = value

        # Create some arrays to speed up later computation.
        self.__set_ratings(self.rating)
        self.total_vp = self.__sum_vp_by_id(self.id, self.id_1, self.vp)

        # Save adjustment factors so they don't need to be recalculated each time.
        adj_factors = self.__calc_combined_adj_factors()

        # Iterate ratings until they converge within tolerance
        while True:
            prev_ratings = numpy.copy(self.rating)
            
            # Iterate the ratings once.
            self.__set_ratings(self.rating * self.total_vp / self.__sum_vp_by_id(self.id, self.id_1, self.__calc_predicted_vp(adj_factors)))

            # Stop iterating if the deviation is within tolerance
            if (numpy.absolute(self.rating - prev_ratings) / self.rating).max() < self.cfg['tolerance']:
                break

        # Normalize ratings to 100
        self.__set_ratings(100 * self.rating / gmean(self.rating))
        return [grp, self.id, self.rating]

    def calc_ratings(self):
        """Retrieve the data from the database."""
        
        # Get database connection information
        with open('lib/yml/db.yml', 'r') as yml:
            self.db_cfg = utils.desymbolize_keys(yaml.safe_load(yml))

        # Get relevant information from database
        with utils.DBConnection(self.db_cfg) as c:
            self.create_vp_temp_table(c)
            with utils.Pool(multiprocessing.cpu_count()) as pool:
                results = []
                for grp in self.groups(c):
                    c.execute(f"""SELECT id, rating FROM `{self.cfg['ratings_table']}` WHERE grp = {grp};""")
                    seed_data = c.fetchall()
                    c.execute(f"""SELECT factor_name, coeff, value FROM `{self.cfg['coeffs_table']}` WHERE grp = {grp} AND factor_name != 'vp_conversion';""")
                    adj_coeff_data = c.fetchall()
                    results.append(pool.apply_async(self.calc_grp_ratings, args=(grp, self.retrieve_vp_table(c, grp), seed_data, adj_coeff_data)))
                for result in results:
                    grp, ids, ratings = result.get()
                    for id, rating in zip(ids, ratings):
                        c.execute(f"""UPDATE `{self.cfg['ratings_table']}` SET rating = {rating} WHERE id = {id};""")


def parse_args():
    """Parse arguments to the program.
    
    Returns the arguments passed to the program.
    """
    p = argparse.ArgumentParser(description='Calculate BTZ ratings.')
    p.add_argument('cfg_file', metavar='C', type=str, nargs=1, help='The YAML configuration file for the BTZ system.')
    return p.parse_args()

def main():
    """Calculate BTZ ratings."""

    # Get the input to the program
    args = parse_args()

    # Calculate the ratings and put them in the database
    brc = BtzRatingsCalculator(args.cfg_file[0], 0)
    brc.calc_ratings()

# Run the program
if __name__ == '__main__':
    main()
