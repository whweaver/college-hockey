require 'mysql'
require 'utils'

STATS_DIRECTORY = File.join(*%w[teams stats])

# Gets a player ID from its name and team, assuming that player is on the roster.
# @param db [Mysql] The mysql connection.
# @param name [String] The player's full name (first last).
# @param team_id [Integer] The ID of the team the player is on.
# @return [Result] The MySQL result containing the ID.
def get_player_id_by_name_raw(db, name, team_id)
  db.query("SELECT players.id
            FROM players
              LEFT JOIN player_aliases ON players.chn_path = player_aliases.chn_path
              INNER JOIN rosters ON players.id = rosters.player_id
            WHERE rosters.team_id = #{team_id.to_sql}
              AND (CONCAT(players.first_name, ' ', players.last_name) = #{name.to_sql}
                OR CONCAT(player_aliases.first_name, ' ', player_aliases.last_name) = #{name.to_sql})").fetch
end

# Gets a player ID from its name and team.
# @param db [Mysql] The mysql connection.
# @param name [String] The player's full name (first last).
# @param team_id [Integer] The ID of the team the player is on.
# @return [Integer] The player's ID.
def get_player_id_by_name(db, name, team_id)
  result = get_player_id_by_name_raw(db, name, team_id)

  if result.nil?
    player_id = nil
    season_id, path_id = db.select('teams', %w[season chn_path_id], {id: team_id}).fetch
    stats_path = "stats/team/#{path_id}/overall,#{season_id}"
    download_db File.join(STATS_DIRECTORY, stats_path.gsub(/[\/,]/, '__')) => "https://www.collegehockeynews.com/#{stats_path}" do |db, html|
      html.css('table#skaters tbody tr').each do |tr_e|
        tds = tr_e.elements
        name_a = tds[0].elements[0]

        wheres = { chn_path: name_a['href'] }
        player_id = db.insert_if_not_exists(:players, %w[id], wheres, wheres.merge(Hash[%w[first_name last_name].zip(name_a.text.strip.split(/\s+/, 2))])).to_i
        
        wheres = {
          team_id: team_id,
          player_id: player_id
        }
        db.insert_if_not_exists(:rosters, %w[team_id player_id], wheres, wheres.merge(Hash[%w[position class].zip(tds[0].text.strip.split(/,\s*/)[1..2])]))[1].to_i
      end
    end.invoke

    result = get_player_id_by_name_raw(db, name, team_id)
    if result.nil?
      db.insert(:players, Hash[%w[first_name last_name].zip(name.split(/\s+/, 2))])
      player_id = db.query("SELECT LAST_INSERT_ID()").fetch[0]
      db.insert(:rosters, {
        team_id: team_id,
        player_id: player_id
      })
      
      player_id
    else
      result[0]
    end
  else
    result[0]
  end
end
