require 'utils'

# Get the team ID from a name and season.
# @param db [Mysql] The database connection.
# @param name [String] The team's name.
# @param season_id [String] The season ID.
# @return [Integer] The team ID.
def get_team_from_name(db, name, season_id)
  db.lock(:schools, :teams) do
    results = db.query("SELECT DISTINCT school_id
                        FROM teams
                          JOIN schools ON teams.school_id = schools.id
                        WHERE teams.name = #{name.to_sql}
                          AND schools.chn_path IS NULL")
    raise "More than one ID for school name #{name}" if results.size > 1
    result = results.fetch
    if result.nil?
      db.insert('schools', {chn_path: nil})
      school_id = db.query('SELECT LAST_INSERT_ID()').fetch[0]
    else
      school_id = result[0]
    end

    wheres = {
      school_id: school_id,
      season: season_id
    }

    db.insert_or_update('teams', wheres.merge({ name: name }), wheres, %w[id])[0].to_i
  end
end

# Get the team ID from a path and season.
# @param db [Mysql] The database connection.
# @param path [String] The team's path.
# @param season_id [String] The season ID.
# @param name [String] The team's name.
# @return [Integer] The team ID.
def get_team_from_path(db, path, season_id, name)
  path_id = path.match(/reports\/team\/(.*)/)[1]
  db.lock(teams: :write, schools: :read) do
    result = db.select(:teams, %w[id], {season: season_id, chn_path_id: path_id}).fetch
    if result.nil?
      db.insert(:teams, {
        season: season_id,
        school_id: db.select(:schools, %w[id], {chn_path: resolve_path(path)}).fetch[0],
        chn_path_id: path_id,
        name: name
      })
      db.query('SELECT LAST_INSERT_ID()').fetch[0].to_i
    else
      team_id = result[0].to_i
      db.update(:teams, {name: name}, {id: team_id})
      team_id
    end
  end
end

# Get the team IDs from a game row.
# @param db [Mysql] The database connection.
# @param tds [Array] The td elements from the game row.
# @param season_id [String] The season ID.
# @param team_path_id_cache [Hash] A cache of the team { path: id }
# @param team_name_id_cache [Hash] A cache of the team { name: id }
# @return [Array] The team IDs.
def get_team_ids(db, tds, season_id, team_path_id_cache, team_name_id_cache)
  Hash[[:team1_id, :team2_id].zip([tds[0], tds[3]].map do |td|
    name = td.text.strip
    if td.elements.empty?
      team_name_id_cache[name] ||= get_team_from_name(db, name, season_id)
    else
      path = td.elements[0]['href']
      team_path_id_cache[path] ||= get_team_from_path(db, path, season_id, name)
    end
  end)]
end

# Gets some metadata about the game
# @param tds [Array] The td elements for the game.
# @param notes [Hash] The notes for the season.
# @return [Hash] The metadata.
def parse_game_meta(tds, notes)
  data = {}
  case tds.length
    when 11
      tds[6].text.match(/(\d+):0*(\d+)\s*([^\s\u00A0]+)/) do |m|
        data[:hour] += 12 if (data[:hour] = m.captures[0].to_i) < 12
        data[:minute] = m.captures[1].to_i
        data[:zone] = m.captures[2]
      end
      data[:note] = notes[tds[10].text]
      data[:box_path] = tds[7].elements[0]['href'] if (!tds[7].elements.empty?) and tds[7].elements[0]['href'] =~ /\/box\/final\//
      data[:metrics_path] = tds[8].elements[0]['href'] if (!tds[8].elements.empty?) and tds[8].elements[0]['href'] =~ /box\/metrics.php\?gd=/
    when 8
      data[:note] = notes[tds[7].text]
    else
      raise 'Unknown schedule line format!'
  end
  data[:note] &&= data[:note].strip
  data
end

# Extracts season data from an HTML page.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
def parse_season(db, html)
  team_name_id_cache = {}
  team_path_id_cache = {}
  date = nil
  type = nil
  game_ids = []
  notes = Hash[html.css('div.comment p')[0].text.scan(/(\d+)\s+(.*)/).map { |m| [m[0], m[1]] }]
  season_id = html.css('div#wrap div#content h1')[0].text.match(/(\d{4})\D+\d{2}/) { |m| "#{m[1]}#{m[1].to_i + 1}" }
  html.css('table.data.schedule.full tbody tr:not(.empty)').each do |tr_e|
    tds = tr_e.elements
    case tr_e['class']
      when 'stats-section'
        date = Date.parse(tds[0].text)

      when 'sked-header'
        type = tr_e.text.strip

      else
        game_meta = parse_game_meta(tds, notes)

        wheres = {
          date_time: "#{date.strftime('%Y/%m/%d')} #{game_meta[:hour]}:#{game_meta[:minute]}",
          time_zone: game_meta[:zone],
          season: season_id
        }.merge(
          get_team_ids(db, tds, season_id, team_path_id_cache, team_name_id_cache)
        )

        db.insert_or_update('games', wheres.merge({
          chn_box_path: game_meta[:box_path],
          chn_metrics_path: game_meta[:metrics_path],
          type: type,
          sked_note: game_meta[:note],
          ot: (tds[5].text.match(/(\d+)?ot/) { |m| m[1].nil? ? 1 : m[1].to_i } or 0)
        }), wheres)
        game_id = db.select('games', %w[id], wheres).fetch[0]
        db.query("UPDATE games SET last_updated = CURRENT_TIMESTAMP() WHERE id = #{game_id.to_sql}")
        game_ids << game_id

        [:team1_id, :team2_id].zip([tds[1], tds[4]]).each do |team_key, goal_td|
          team_game_wheres = {
            game_id: game_id,
            team_id: wheres[team_key]
          }
          db.insert_or_update('team_games', team_game_wheres.merge({
            goals: goal_td.text.strip.to_i
          }), team_game_wheres)
        end
    end
  end

  db.query("SELECT id, chn_box_path, chn_metrics_path FROM games WHERE season = #{season_id} AND id NOT IN (#{game_ids.join(', ')})").each do |id, box_path, metrics_path|
    [BOX_DIRECTORY, METRICS_DIRECTORY].zip([box_path, metrics_path]).each do |dir, filename|
      unless filename.nil?
        html_path = File.join(dir, filename.gsub('/', '__')).html_path
        FileUtils.rm_rf(html_path) if File.exist?(html_path)
        db_dummy_path = File.join(dir, filename.gsub('/', '__')).db_dummy_path
        FileUtils.rm_rf(db_dummy_path) if File.exist?(db_dummy_path)
      end
    end
  end
  db.query("DELETE FROM games WHERE season = #{season_id} AND id NOT IN (#{game_ids.join(', ')})")
end
