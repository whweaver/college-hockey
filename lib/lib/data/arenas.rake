rake_require 'utils'
require 'arena_utils'

ARENA_LIST_LOCATION = File.join(*%w[arenas arena_list.html])

setup_season *[ARENA_LIST_LOCATION, ARENAS_DIRECTORY].map { |p| p.html_path }

download_db ARENA_LIST_LOCATION => 'https://www.collegehockeynews.com/almanac/arenas-index.php' do |db, html|
  html.css('table.data.sortable tbody tr').each do |tr_e|
    tds = tr_e.css('td')
    assns = {
      year_built: tds[2].text,
      year_closed: tds[3].text,
    }
    assns[:city], assns[:state] = (tds[1].text.match(/([^,]*)(?:,\s*(.*))?/).captures unless tds[1].text.empty?)
    assns[:start_active_year], assns[:end_active_year] = tds[4].text.split('-')

    chn_path = tds[0].css('a')[0]['href']

    add_arena(db, tds[0].text, chn_path)
    db.update('arenas', assns, { chn_path: chn_path })
  end
end

db_multitask({:parse_arenas => ARENA_LIST_LOCATION.db_dummy_path}, 'SELECT id, chn_path FROM arenas') do |arena_id, arena_path|
  db_update { |db| dl_and_parse_arena(db, arena_id, arena_path) }
end

desc 'Update all arena data'
task :update_arenas => :setup_season do
  Rake::Task[:parse_arenas].invoke
end
