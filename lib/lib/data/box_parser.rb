require 'game_utils'

# Parses the meta section of the game box score.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
# @param game_id [Integer] The game ID in the database.
def parse_box_meta(db, html, game_id)
  subtext = html.css('div#meta')[0].elements[2]

  arena_text = subtext.elements[1].text
  arena_text = subtext.elements[2].text if arena_text.include?('shootout') and not (arena_text =~ /\s+at\s+/)
  if m = arena_text.match(/.*Game\s*(.*)\s+at\s+.*\s+at\s+(\S.*),\s*(\S.*),\s*(\S.*)/)
    arena_assns = {
      name: m.captures[1].strip,
      city: m.captures[2].strip,
      state: m.captures[3].strip
    }
  elsif m = arena_text.match(/.*Game\s*(.*)\s+at\s+(\S.*\s+at\s+\S.*),\s*(\S.*)/)
    name = m.captures[1]
    first_at, second_at = name.match(/(.*\S)\s+at\s+(\S.*)/).captures
    if first_at == second_at
      arena_assns = {
        name: second_at,
        city: m.captures[2].strip,
        state: nil
      }
    else
      arena_assns = {
        name: name,
        city: m.captures[2].strip,
        state: nil
      }
    end
  elsif m = arena_text.match(/.*Game\s*(.*)\s+at\s+(\S.*),\s*(\S.*)\s*at\s+(\S.*),/)
    arena_assns = {
      name: m.captures[3].strip,
      city: m.captures[1].strip,
      state: m.captures[2].strip
    }
  elsif m = arena_text.match(/.*Game\s*(.*)\s+at\s+(\S.*),\s*(\S.*),\s*(\S.*)/)
    arena_assns = {
      name: m.captures[1].strip,
      city: m.captures[2].strip,
      state: m.captures[3].strip
    }
  elsif m = arena_text.match(/.*Game\s*(.*)\s+at\s+(\S[^\(]*),\s*(\S.*)/)
    arena_assns = {
      name: m.captures[1].strip,
      city: m.captures[2].strip,
      state: nil
    }
  elsif m = arena_text.match(/.*Game\s*(.*)\s+at\s+(.*),/)
    arena_assns = {
      name: m.captures[1].strip,
      city: nil,
      state: nil
    }
  else
    raise "Cannot parse the following text:\n#{arena_text}"
  end

  if arena_assns[:name].empty?
    arena_id = nil
  else
    result = db.select(:arenas, %w[id], arena_assns).fetch
    if result.nil?
      result = db.query("SELECT id
                         FROM arenas
                         WHERE name = #{arena_assns[:name].to_sql}
                           AND city IS NULL
                           AND state IS NULL").fetch
      if result.nil?
        results = db.query("SELECT id
                         FROM arenas
                         WHERE name = #{arena_assns[:name].to_sql}")
        raise "Multiple arenas with name '#{arena_assns[:name]}'!" if results.size > 1
        result = results.fetch
        raise "Unrecognized arena #{arena_assns}!" if result.nil?
        result[0]
      else
        arena_id = result[0]
        db.update(:arenas, arena_assns, { id: arena_id })
      end
    else
      arena_id = result[0]
    end
  end

  db.update('games', {
    arena_id: arena_id,
    box_note: m.captures[0].strip,
    attendance: subtext.elements[2].text.match(/Attendance:\s*(\S*)/) { |m| m.captures[0].gsub(/\D/, '').to_i }
  }, {
    id: game_id
  })

  subtext.elements[2].text.match(/Referees?: (?:(.*),\s*)*(.*)\s*(Asst. Referees?:) (?:(.*),\s*)*(.*)/) do |m|
    assistant = false
    m.captures.each do |c|
      if c =~ /Asst. Referees?:/
        assistant = true
      elsif not c.nil?
        first_name, last_name = c.split(/\s+/, 2)

        ref_game_wheres = {
          game_id: game_id,
          referee_id: db.insert_if_not_exists('referees', %w[id], {
            first_name: first_name,
            last_name: last_name
          })
        }

        db.insert_or_update('referee_games', ref_game_wheres.merge({role: assistant ? 'asst' : 'ref'}), ref_game_wheres)
      end
    end
  end
end

# Parses the linescore section of the game box score.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
# @param game_id [Integer] The game ID in the database.
# @param team_ids [Array] An array of the team IDs.
# @return [Array, Integer] An array of the team abbreviations, number of periods
def parse_box_linescore(db, html, game_id, team_ids)
  wheres = { game_id: game_id }
  max_period = 0
  trs = html.css('div#linescore div#shots table tbody tr')
  trs.zip(team_ids).each do |shots_e, team_id|
    wheres[:team_id] = team_id
    shots_e.elements[1..-2].to_enum.with_index(1) do |period_shots_e, period|
      wheres[:period] = period
      max_period = period
      db.insert_or_update('team_periods', wheres.merge({shots: period_shots_e.text.strip.to_i}), wheres)
    end
  end
  [(trs.map { |tr| tr.elements[0].text.strip }), max_period]
end

# Parses the scoring section of the game box score.
# @param html [Nokogiri::HTML] The page's HTML data.
def parse_box_scoring(db, html, game_id, team_ids, team_abbrs, num_periods)
  wheres = { game_id: game_id }
  html.css('div#scoring table tr').each do |tr_e|

    # Enter new period
    if tr_e['class'] == 'stats-section'
      period_text = tr_e.elements[0].text
      if period_text =~ /^(\d+).*Period$/
        wheres[:period] = $1.to_i
      elsif period_text == 'Overtime'
        wheres[:period] = num_periods
      end

    # Found a goal
    elsif tr_e['class'] =~ /\wscore/
      wheres[:game_time] = "00:#{tr_e.elements[5].text}"

      scorer_a = tr_e.elements[3].elements[0]
      scorer_path = scorer_a['href']
      assist_ids = tr_e.elements[4].text.match(/(?:([^,]*),\s*)*([^,]*)/) { |m| m.captures.map { |c| c.strip unless c.nil? }.compact }.map do |assist_name|
        get_player_id_by_name(db, assist_name, team_ids[team_abbrs.index(tr_e.elements[0].text.strip)]) unless assist_name.empty?
      end.compact

      db.insert_or_update('goals', wheres.merge({
        type: tr_e.elements[1].text.strip,
        scorer_id: (db.select('players', %w[id], {chn_path: scorer_path}).fetch[0] unless (scorer_path.nil? or scorer_a.text.strip.empty?)),
        assist1_id: assist_ids[0],
        assist2_id: assist_ids[1]
      }), wheres)
    end
  end
end

# Parses the penalties section of the game box score.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
# @param game_id [Integer] The game ID in the database.
# @param team_ids [Array] An array of the team IDs.
# @param team_abbrs [Array] An array of the team abbreviations.
def parse_box_penalties(db, html, game_id, team_ids, team_abbrs)
  wheres = { game_id: game_id }
  html.css('div#penalties table tr').each do |tr_e|

    # Found a new period
    if tr_e['class'] == 'stats-section'
      wheres[:period] = $1.to_i if tr_e.elements[0].text.strip =~ /^(\d+).*Period$/

    # Found a penalty
    elsif tr_e['class'] =~ /\wscore/
      player_name = tr_e.elements[1].text.strip
      wheres.merge!({
        player_id: (get_player_id_by_name(db, player_name, team_ids[team_abbrs.index(tr_e.elements[0].text.strip)]) unless player_name.upcase == 'BENCH'),
        duration: tr_e.elements[2].text.strip.to_i,
        type: tr_e.elements[3].text.strip,
        game_time: "00:#{tr_e.elements[4].text}"
      })
      db.insert_or_update('penalties', wheres, wheres)
    end
  end
end

# Parses the goalies section of the game box score.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
# @param game_id [Integer] The game ID in the database.
# @param team_ids [Array] An array of the team IDs.
def parse_box_goalies(db, html, game_id, team_ids)
  wheres = { game_id: game_id }
  html.css('div#goalies table tbody').zip(team_ids).each do |goalie_table, team_id|
    goalie_table.elements.each do |tr_e|
      tds = tr_e.elements
      name = tds[0].text.strip

      if name.upcase != 'EMPTY NET'
        wheres[:player_id] = get_player_id_by_name(db, name, team_id)
        db.insert_or_update('goalie_games', wheres.merge({
          total_saves: tds[1].text.to_i,
          total_goals: tds[2].text.to_i
        }), wheres)
      end
    end
  end
end

# Parses the skaters sectino of the game box score.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
# @param game_id [Integer] The game ID in the database.
# @param team_ids [Array] An array of the team IDs.
def parse_box_skaters(db, html, game_id, team_ids, team_abbrs)
  wheres = { game_id: game_id }
  html.css('div#playersums div.playersum table.data').each do |player_table|
    team_id = team_ids[team_abbrs.index(player_table.css('thead tr td')[0].text)]
    player_table.css('tbody tr').each do |tr_e|
      wheres[:player_id] = get_player_id_by_name(db, tr_e.elements[0].text.strip, team_id)

      if tr_e.elements[7].nil?
        fow = nil
        fol = nil
      else
        fow, fol = (tr_e.elements[7].text.strip.match(/(\d+)(?:\D.*\D|\D)(\d+)/) { |m| m.captures.map(&:to_i) } or [0, 0])
      end

      db.insert_or_update('skater_games', wheres.merge({
        plusminus: tr_e.elements[4].text.strip.to_i,
        fow: fow,
        fol: fol
      }), wheres)
    end
  end
end

# Parses the game box score into the database.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
# @param game_id [Integer] The ID of the game being parsed.
def parse_box(db, html, game_id)
  if html.css('div#content h2')[0].text != 'Game Not Available'
    results = db.select('games', %w[team1_id team2_id], {id: game_id})
    team_ids = results.fetch
    parse_box_meta(db, html, game_id)
    team_abbrs, num_periods = parse_box_linescore(db, html, game_id, team_ids)
    parse_box_scoring(db, html, game_id, team_ids, team_abbrs, num_periods)
    parse_box_penalties(db, html, game_id, team_ids, team_abbrs)
    parse_box_goalies(db, html, game_id, team_ids)
    parse_box_skaters(db, html, game_id, team_ids, team_abbrs)
  end
end
