rake_require 'utils'
rake_require 'coaches'
rake_require 'arenas'
rake_require 'teams'
rake_require 'players'
rake_require 'games'

desc 'Clear out the data in the database and recreate it'
task :setup_db_data do
  FileUtils.rm_rf('data/db_dummies'.to_absolute)
  `mysql -u hockey_updater -p < sql/setup_data.sql`
end

desc 'Update all data'
multitask :update_data => %w[update_coaches
                             update_arenas
                             update_teams
                             update_players
                             update_games]
