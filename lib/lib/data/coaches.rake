rake_require 'utils'
require 'coach_utils'

COACH_LIST_LOCATION = File.join(*%w[coaches coach_list.html])

setup_season *[COACH_LIST_LOCATION, COACHES_DIRECTORY].map{ |p| p.html_path }

download_db COACH_LIST_LOCATION => 'https://www.collegehockeynews.com/almanac/coach-alltime.php' do |db, html|
  html.css('table.stats.data.sortable tbody tr').each do |tr_e|
    tds = tr_e.css('td')
    last_name, first_name = tds[1].text.split(',').map(&:strip)
    add_coach(db, first_name, last_name, tds[1].css('a')[0]['href'])
  end
end

access_db :parse_coaches => COACH_LIST_LOCATION.db_dummy_path do |t, db|
  immediate_multitask :parse_coach_pages => (db.select('coaches', %w[id chn_path], {}).map { |coach_id, coach_path| dl_and_parse_coach(db, coach_id, coach_path) })
end

desc 'Update all coach data'
task :update_coaches => :setup_season do
  Rake::Task[:parse_coaches].invoke
end
