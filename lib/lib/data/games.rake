require_relative 'season_parser'
require_relative 'box_parser'
require_relative 'metrics_parser'
rake_require 'utils'
rake_require 'arenas'

SCHEDULE_LIST_LOCATION = File.join(*%w[schedules schedule_list.html])
SEASONS_DIRECTORY = File.join(*%w[schedules seasons])
BOX_DIRECTORY = File.join(*%w[schedules boxes])
METRICS_DIRECTORY = File.join(*%w[schedules metrics])

# Get games that started at least 'interval' ago, and have not been updated
# since that interval passed.
# @param db [Mysql] The database connection.
# @param interval [String] The SQL interval in the form '6 HOUR'
# @return [ResultSet] The game IDs that should be updated.
def get_not_updated_games(db, interval)
  db.query("SELECT id, chn_box_path, chn_metrics_path
            FROM games
            WHERE (NOW() > DATE_ADD(date_time, INTERVAL #{interval}))
              AND (last_updated < DATE_ADD(date_time, INTERVAL #{interval}))")
end

setup_season SCHEDULE_LIST_LOCATION.html_path

access_db :force_game_refreshes do |t, db|
  ['6 HOUR', '5 DAY'].each do |interval|
    results = get_not_updated_games(db, interval)
    cur_season_html_path = File.join(SEASONS_DIRECTORY, cur_season).html_path
    if results.size > 0
      FileUtils.rm_rf(cur_season_html_path) if File.exist?(cur_season_html_path)
      FileUtils.rm_rf(ARENA_LIST_LOCATION.html_path) if File.exist?(ARENA_LIST_LOCATION.html_path)
    end
    results.each do |result|
      [BOX_DIRECTORY, METRICS_DIRECTORY].zip(result[1..2]).each do |dir, filename|
        unless filename.nil?
          html_path = File.join(dir, filename.gsub('/', '__')).html_path
          FileUtils.rm_rf(html_path) if File.exist?(html_path)
        end
      end
    end
  end
end

download_db SCHEDULE_LIST_LOCATION => %w[https://www.collegehockeynews.com/schedules] do |db, html|
  html.css('ul.ContextBar li.index.hasmore div ul.linear li a').each do |a_e|
    assns = {
      id: a_e.text.strip.match(/(\d{4})\D+\d{2}/) { |m| "#{m[1]}#{m[1].to_i + 1}" },
      chn_path: a_e['href']
    }
    db.insert_or_update('seasons', assns, assns)
  end
end

db_multitask({:parse_schedules => [SCHEDULE_LIST_LOCATION.db_dummy_path, :update_teams]}, "SELECT id, chn_path FROM seasons") do |season_id, season_path|
  download_db File.join(SEASONS_DIRECTORY, season_id) => "https://www.collegehockeynews.com/schedules/?season=#{season_id}" do |db, html|
    parse_season(db, html)
  end
end

db_multitask({:parse_games => %w[parse_schedules update_arenas update_players]}, "SELECT id, chn_box_path, chn_metrics_path
                                                                                  FROM games
                                                                                  WHERE chn_box_path IS NOT NULL
                                                                                    OR chn_metrics_path IS NOT NULL") do |game_id, box_path, metrics_path|
  box_downloader = nil
  metrics_deps = []

  unless box_path.nil?
    box_downloader = download_db File.join(BOX_DIRECTORY, box_path.gsub('/', '__')) => "https://www.collegehockeynews.com/#{box_path}" do |db, html|
      parse_box(db, html, game_id)
    end
    metrics_deps << box_downloader
  end

  if metrics_path.nil?
    box_downloader
  else
    metrics_deps << "https://www.collegehockeynews.com/#{metrics_path}"
    download_db File.join(METRICS_DIRECTORY, metrics_path.gsub('/', '__')) => metrics_deps do |db, html|
      parse_metrics(db, html, game_id)
    end
  end
end

desc 'Update game data'
multitask :update_games => %w[setup_season force_game_refreshes] do
  Rake::Task[:parse_games].invoke
end
