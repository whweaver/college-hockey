rake_require 'utils'

COACHES_DIRECTORY = File.join(*%w[coaches coaches])

def add_coach(db, first_name, last_name, chn_path)
  assns = {
    first_name: first_name,
    last_name: last_name,
    chn_path: chn_path
  }
  db.insert_or_update('coaches', assns, {chn_path: assns[:chn_path]})
end

def dl_and_parse_coach(db, coach_id, coach_path)
  download_db File.join(COACHES_DIRECTORY, coach_path.gsub(/\//, '__')) => "https://www.collegehockeynews.com/#{coach_path}" do |db, html|
    divs = html.css('div.factbox.bio div.gridinfo div')
    divs_1_match = divs[1].text.match(/([^\(]*)(?:\((\d{4})\))?/)
    assns = {}
    assns[:alma_mater], assns[:grad_year] = (divs_1_match.captures unless divs_1_match.nil?)
    if divs[3].text.match(/^0+\/|\/0+\//)
      assns[:birthdate] = Date.new(divs[3].text.match(/\/(\d+)$/)[0].to_i)
    else
      assns[:birthdate] = (Date.strptime(divs[3].text.strip, '%m/%d/%Y') unless divs[3].text.empty?)
    end
    assns[:hometown_city], assns[:hometown_state] = (divs[5].text.match(/([^,]*)(?:,\s*(.*))?/).captures unless divs[5].text.empty?)

    db.update('coaches', assns, { id: coach_id})
  end
end