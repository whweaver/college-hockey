rake_require 'utils'
require 'coach_utils'
require 'arena_utils'

SCHOOL_LIST_LOCATION = File.join(*%w[schools school_list.html])
HISTORIES_DIRECTORY = File.join(*%w[schools histories])

setup_season *[SCHOOL_LIST_LOCATION, HISTORIES_DIRECTORY].map { |p| p.html_path }

download_db SCHOOL_LIST_LOCATION => %w[https://www.collegehockeynews.com] do |db, html|
  html.css('li#teamflyout div div div').each do |div_e|
    div_e.css('p').each do |p_e|
      path = p_e.css('a')[0]['href']
      db.insert_or_update('schools', {chn_path: path}, {chn_path: path})
    end
  end
end

db_multitask({:parse_histories => [SCHOOL_LIST_LOCATION.db_dummy_path, :update_coaches, :update_arenas]},
             'SELECT id, chn_path FROM schools WHERE chn_path IS NOT NULL') do |school_id, school_path|
  team_id = school_path.match(/reports\/team\/(.*)/)[1]
  download_db File.join(HISTORIES_DIRECTORY, team_id.gsub(/\//, '__')) => "https://www.collegehockeynews.com/reports/teamHistory/#{team_id}" do |db, html|
    division = nil
    nickname = nil
    conference = nil
    season = nil
    chn_path_id = nil
    name = html.css('h2.teamlabel')[0].text.match(/(.*)\s*Team\s*History/)[1].strip
    html.css('table.data tbody tr').each do |tr_e|
      if tr_e['class'] == 'stats-section'
        tr_e.text.match(/(.*)\s*\|\s*Division: (I+) \((.*)\)/) do |m| 
          nickname = m[1].strip
          division = m[2].length
          conference = m[3]
        end
      else
        tds = tr_e.css('td')
        tds[0].elements[0].tap do |a_e|
          chn_path_id, season = a_e['href'].match(/\/schedules\/team\/(.*)\/(\d{8})/).captures unless a_e.nil?
        end
        coach_path = (tds[5].css('a')[0]['href'].strip unless (tds[5].text.strip.match(/^No Coach$/)) or tds[5].text.empty?)
        arena_path = (tds[9].css('a')[0]['href'] unless tds[9].css('a')[0].text.empty?)

        if coach_path.nil?
          coach_id = nil
        else
          result = db.select('coaches', %w[id], {chn_path: coach_path}).fetch
          if result.nil?
            first_name, last_name = tds[5].text.strip.split(/\s/)
            add_coach(db, first_name, last_name, coach_path)
            result = db.select('coaches', %w[id chn_path], {chn_path: coach_path}).fetch
            coach_id = result[0].to_i
            dl_and_parse_coach(db, coach_id, result[1]).invoke
          else
            coach_id = result[0].to_i
          end
        end

        if arena_path.nil?
          arena_id = nil
        else
          result = db.select('arenas', %w[id], {chn_path: arena_path}).fetch
          if result.nil?
            add_arena(db, tds[9].text.strip, arena_path)
            result = db.select('arenas', %w[id chn_path], {chn_path: arena_path}).fetch
            arena_id = result[0].to_i
            dl_and_parse_arena(db, arena_id, result[1]).invoke
          else
            arena_id = result[0].to_i
          end
        end

        assns = {
          school_id: school_id,
          name: name,
          season: season,
          arena_id: arena_id,
          nickname: nickname,
          conference: conference,
          wins: tds[1].text.strip.to_i,
          losses: tds[2].text.strip.to_i,
          ties: tds[3].text.strip.to_i,
          chn_path_id: chn_path_id,
          has_schedule: true,
          reg_season_champs: tds[6]['class'].include?('check'),
          conf_tourn_champs: tds[7]['class'].include?('check'),
          ncaa_tourn: tds[8]['class'].include?('check')
        }

        wheres = {
          school_id: school_id,
          season: season,
          arena_id: arena_id
        }

        team_id = db.insert_or_update('teams', assns, wheres, %w[id])[0]
        db.insert_if_not_exists('team_coaches', %w[team_id coach_id], {team_id: team_id, coach_id: coach_id}) unless coach_id.nil?

      end # if tr_e['class'] == 'stats-section'
    end # html.css('table.data tbody tr').each do |tr_e|
  end # download_db
end

desc 'Update team data'
multitask :update_teams => :setup_season do
  Rake::Task[:parse_histories].invoke
end
