require 'utils'

def get_skater_row_assns(tr_e)
  assns = {}
  [[:total, :tot], [:even, :evs], [:pp, :ppl], [:close, :cls]].each do |situation, clss|
    tr_e.css("td.#{clss}").zip([:blocked_shots, :wide_shots, :hit_posts, :saved_shots, :goals, :shot_attempts]) do |td_e, result|
      assns["#{situation}_#{result}"] = td_e.text.strip.to_i if result != :goals
    end
  end
  assns[:blocks] = tr_e.css('td:not([class])')[0].text.strip.to_i
  assns
end

# Parses the game metrics.
# @param db [Mysql] The database to insert the data into.
# @param html [Nokogiri::HTML] The page's HTML data.
# @param game_id [Integer] The game ID in the database.
def parse_metrics(db, html, game_id)
  results = db.select('games', %w[team1_id team2_id], {id: game_id})
  team_ids = results.fetch
  team_abbrs = []

  # Parse skater charts
  html.css('div.widetableWrap table.sortable.metrics').zip(team_ids).each do |table_e, team_id|
    team_abbrs << table_e.css('thead tr td')[0].text

    # Parse skaters
    table_e.css('tbody tr').each do |tr_e|
      name = tr_e.elements[0].text
      if name.upcase != 'TEAM' and not name.empty?
        wheres = {
          game_id: game_id,
          player_id: get_player_id_by_name(db, name, team_id)
        }
        assns = get_skater_row_assns(tr_e)
        db.insert_or_update('skater_games', wheres.merge(assns), wheres)
      end
    end

    # Parse team totals
    team_tr = table_e.css('tfoot tr')[0]
    assns = get_skater_row_assns(team_tr)
    db.update('team_games', assns, {game_id: game_id, team_id: team_id})
  end

  # Parse goalie charts
  html.css('table.metrics.sortable[width="50%"]').each do |table_e|
    team_id = team_ids[team_abbrs.index(table_e.css('thead tr td')[0].text)]
    table_e.css('tbody tr').each do |tr_e|
      name = tr_e.elements[0].text
      if name.upcase != 'EMPTY NET'
        wheres = {
          game_id: game_id,
          player_id: get_player_id_by_name(db, name, team_id)
        }
        tds = tr_e.elements
        db.insert_or_update('goalie_games', wheres.merge({
          total_saves: tds[1].text.to_i,
          total_goals: tds[2].text.to_i,
          even_saves: tds[4].text.to_i,
          even_goals: tds[5].text.to_i,
          sh_saves: tds[7].text.to_i,
          sh_goals: tds[8].text.to_i,
          close_saves: tds[10].text.to_i,
          close_goals: tds[11].text.to_i
        }), wheres)
      end
    end
  end
end
