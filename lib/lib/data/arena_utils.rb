rake_require 'utils'

ARENAS_DIRECTORY = File.join(*%w[arenas arenas])

def add_arena(db, name, chn_path)
  assns = {
    name: name,
    chn_path: chn_path
  }

  db.insert_or_update('arenas', assns, { chn_path: chn_path })
end

def dl_and_parse_arena(db, arena_id, arena_path)
  download_db File.join(ARENAS_DIRECTORY, arena_path.gsub('/', '__')) => "https://www.collegehockeynews.com/#{arena_path}" do |db, html|
    divs = html.css('div.factbox div.gridinfo div')
    assns = {
      capacity: divs[3].text.gsub(/,/, '').to_i,
      street_addr: divs[7].text.split(',')[0]
    }
    assns[:sheet_length], assns[:sheet_width] = divs[5].text.split('x').map(&:to_i)

    db.update('arenas', assns, { id: arena_id })
  end
end