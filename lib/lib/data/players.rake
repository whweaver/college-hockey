rake_require 'utils'

ROSTERS_DIRECTORY = File.join(*%w[teams rosters])
PLAYERS_DIRECTORY = File.join(*%w[players players])

db_multitask({:parse_rosters => :update_teams}, 'SELECT schools.chn_path, teams.season, teams.id
                                               FROM schools
                                                 JOIN teams ON schools.id = teams.school_id
                                               WHERE schools.chn_path IS NOT NULL
                                                 AND teams.has_schedule = TRUE') do |school_path, season, team_id|
  wheres = { team_id: team_id }
  team_path_id = school_path.match(/reports\/team\/(.*)/)[1]
  roster_id = "#{team_path_id}/#{season}"
  download_db File.join(ROSTERS_DIRECTORY, roster_id.gsub('/', '__')) => "https://www.collegehockeynews.com/reports/roster/#{roster_id}" do |db, html|
    html.css('table#players tbody tr:not(.stats-section)').each do |tr_e|
      tds = tr_e.elements
      last_name, first_name, captain = tds[2].text.strip.match(/^(.*),\s*(.*?)\s*(?:\((.)\))?$/)[1..3]
      db.lock(:players) do
        players_wheres = { chn_path: tds[2].elements[0]['href'] }
        wheres[:player_id] = db.insert_if_not_exists('players', %w[id], players_wheres, players_wheres.merge({
          first_name: first_name,
          last_name: last_name
        }))
      end
      db.insert_or_update('rosters', wheres.merge({
        num: tds[1].text.strip.to_i,
        height: tds[5].text.strip.match(/(\d+)\D+(\d+)/) { |m| m[1].to_i * 12 + m[2].to_i },
        weight: tds[6].text.strip.to_i,
        position: tds[4].text,
        class: (tds[3].text.strip unless tds[3].text.strip.empty?),
        captain: captain
      }), wheres)
    end
  end
end

db_multitask({:parse_players => :parse_rosters}, 'SELECT id, chn_path FROM players WHERE chn_path IS NOT NULL') do |player_id, player_path|
  wheres = { id: player_id }
  download_db File.join(PLAYERS_DIRECTORY, player_path.gsub('/', '__')) => "https://www.collegehockeynews.com/#{player_path}" do |db, html|
    unless html.css('div#content h1')[0].text.match(/\bNo\s+Player\s+Found\b/)
      divs = html.css('div.factbox.bio div.gridinfo div')
      city, state = divs[3].text.match(/([^,]*),([^,]*)/).captures
      month, day, year = divs[1].text.match(/(\d*)\/(\d*)\/(\d*)/).captures.map(&:to_i).map { |d| d == 0 ? 1 : d }
      month = (month > 12) ? 1 : month
      day = (day > 31) ? 1 : day
      begin
        birthdate = (Date.new(year, month, day) unless year == 1)
      rescue ArgumentError => e
        (Date.new(year, month, 1) unless year == 1)
      end
      db.update('players', {
        hometown_city: city,
        hometown_state: state,
        birthdate: birthdate
      }, wheres)
    end
  end
end

desc 'Update player data'
task :update_players => :setup_season do
  Rake::Task[:parse_players].invoke
end
