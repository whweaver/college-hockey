require 'btz_utils'
require 'erb'

# Provides functions for calculating BTZ ratings.
class BtzGroupCalculator

  # Set the groups to make for valid BTZ ratings.
  def calculate()
    db_update do |db|
      # Assign each team to its own group
      # Setup each group with no victory points against any other
      @entity_groups = {}
      @group_has_vp_against = {}
      db.query("SELECT id, grp FROM `#{@ratings_table}`").each_with_index do |(id, grp), i|
        @entity_groups[id] = grp
        @group_has_vp_against[grp] ||= []
      end

      # For each game, either mark a group having victory points against another, or merge the groups
      # if they both have victory points against each other.
      db.query("SELECT * FROM (#{@metric_query}) AS mt").each_hash do |h|
        g1 = @entity_groups[h['id_1']]
        g2 = @entity_groups[h['id_2']]

        if g1 == nil or g2 == nil
          pp h
        end

        # If both teams are in the same group, we don't need to process the game.
        add_vp(g1, g2, h) if g1 != g2
      end

      # Consolidate all groups
      consolidate_groups(db)
    end
  end

  private

  # Initializer.
  # @param cfg The configuration for this BTZ rating system.
  def initialize(cfg)
    @ratings_table = cfg[:ratings_table]
    @metric_query = BtzUtils.erb_from_cfg(File.join(%w[btz full_metric_query.sql.erb]), cfg)
    @transform = cfg[:transform]
  end

  # Add any relevant VP to the given groups.
  # @param g1 [Ingeger] The first group to compare.
  # @param g2 [Integer] The second group to compare.
  # @param h [Hash] The query row.
  def add_vp(g1, g2, h)
    if(@transform)
      add_vp_against(g1, g2)
      add_vp_against(g2, g1)
    else
      add_vp_against(g1, g2) if h['metric'].to_f > 0
      add_vp_against(g2, g1) if h['metric'].to_f < 1
    end
  end

  # Merge 2 groups into a single group.
  # @param db [Mysql] The database connection.
  # @param g1 [Integer] The first group to merge.
  # @param g2 [Integer] The second group to merge.
  def merge_groups(db, g1, g2)
    @entity_groups.transform_values! { |g| (g == g2) ? g1 : g }
    @group_has_vp_against[g2].delete(g1)
    @group_has_vp_against[g1].delete(g2)
    @group_has_vp_against[g1] |= @group_has_vp_against.delete(g2)
    @group_has_vp_against.transform_values! { |groups| groups.map { |g| (g == g2) ? g1 : g }.uniq }
    db.update(@ratings_table, {grp: g1}, {grp: g2})
  end

  # Consolidates any groups that have victory points against each other
  # @param db [Mysql] The database connection.
  def consolidate_groups(db)
    consolidated = true
    while consolidated
      @group_has_vp_against.keys.each do |g|
        consolidated = true
        break if consolidate_groups_helper(db, [g])
        consolidated = false
      end
    end
  end

  # Recursively consolidates any groups that have victory points against each other
  # @param db [Mysql] The database connection.
  def consolidate_groups_helper(db, visited_groups)
    group = visited_groups.last

    # Determine if we've looped to any of the visited groups and merge them if so
    visited_groups.each_with_index do |visited_group, i|
      if @group_has_vp_against[group].include?(visited_group)
        visited_groups[i+1..-1].each do |group_to_merge|
          merge_groups(db, visited_group, group_to_merge)
        end
        return true
      end
    end

    # Try furthering the chain
    @group_has_vp_against[group].each do |g|
      return true if consolidate_groups_helper(db, visited_groups + [g])
    end

    false
  end

  # Add victory points for one group against another.
  # @param g1 [Integer] The group that has victory points against the other.
  # @param g2 [Integer] The group that the other has victory points against.
  def add_vp_against(g1, g2)
    @group_has_vp_against[g1].push(g2) unless @group_has_vp_against[g1].include?(g2)
  end

end
