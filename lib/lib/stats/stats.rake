rake_require 'utils'
rake_require 'location'
rake_require 'btz'

db_rule(/^csv:\w*$/) do |t, db|
  table_to_csv(db, t.name.gsub('csv:', ''))
end

db_rule(/^pivot_csv:\w*$/, [:series, :x, :y]) do |t, db, args|
  table = t.name.gsub('pivot_csv:', '')

  # Get xs
  x_col = args[:x]
  xs = db.query("SELECT DISTINCT(`#{x_col}`) FROM `#{table}` ORDER BY `#{x_col}` ASC").map { |r| r[0] }

  # Setup each series
  series_col = args[:series]
  series = db.query("SELECT DISTINCT(`#{series_col}`) FROM `#{table}`").map { |r| r[0] }
  data = Hash[series.map { |s| [s, []] }]

  # Sort out all the data
  y_col = args[:y]
  db.select(table).each_hash { |r| data[r[series_col]][xs.index(r[x_col])] = r[y_col] }

  FileUtils.mkdir_p('stats')
  File.open(File.join('stats', "#{table}_#{series_col}_#{x_col}_#{y_col}".ext('csv')), 'w') do |f|
    f << [x_col, *series].join(',') + "\n"
    xs.each_with_index { |x, i| f << [x, *series.map { |s| data[s][i] }].join(',') + "\n" }
  end
end
