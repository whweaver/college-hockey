require 'erb'

# Provides functions for calculating BTZ ratings.
class BtzUtils

  # Defaults the values in a configuration.
  # @param cfg [Hash] The configuration to default
  # @return [Hash] The configuration with defaults
  def self.default_cfg(cfg)
    new_cfg = cfg.dup
    new_cfg[:adjustments] ||= []
    new_cfg
  end

  # Get the adjustment coefficient names for this configuration.
  # @param cfg [Hash] The BTZ configurarion.
  # @return [Array<String, String, Integer>] The polynomial names.
  def self.adj_coeffs(cfg)
    coeffs = []
    cfg[:adjustments].each do |adj_name, adj_hsh|
      (0...{
        constant: 1,
        linear: 2,
        quadratic: 3,
        exponential: 2
      }[adj_hsh[:regression_type].to_sym]).each do |coeff_idx|
        coeffs << [adj_name, coeff_idx]
      end
    end
    coeffs
  end

  # Renders an ERB with the given configuration.
  # @param path_to_erb [String] The path to the ERB to render.
  # @param undefaulted_cfg [Hash] The configuration to render the ERB with.
  # @return [String] The rendered contents of the ERB.
  def self.erb_from_cfg(path_to_erb, undefaulted_cfg)
    cfg = self.default_cfg(undefaulted_cfg)
    begin
      ERB.new(File.read(File.join(*%w[lib erb], path_to_erb)), nil, "<>").result(binding)
    rescue StandardError => e
      puts "Failed ERB was '#{path_to_erb}'"
      raise e
    end
  end

end
