# Provides functions for calculating BTZ ratings.
class BTZ

  private

  # Predict once the ratings are known to be valid.
  # @param db [Mysql] The database connection.
  # @param r1 [Float] Rating for team 1.
  # @param r2 [Float] Rating for team 2.
  # @return [Numeric] The predicted metric, if transformed, otherwise, the predicted victory points.
  def predict_from_ratings(db, r1, r2)
    expected_vp = r1.to_f / (r1.to_f + r2.to_f)
    if @transform
      db.query("SELECT logit(coeff, intercept, #{expected_vp})
                FROM  vp_coeffs
                WHERE type = '#{@type}'").fetch.first
    else
      expected_vp
    end
  end

end
