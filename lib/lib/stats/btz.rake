# Mathematical Source: http://elynah.com/tbrw/tbrw.cgi?krach
#
# OVERVIEW
# --------
#
# This file contains a partial implementation of BTZ ratings. This rating system is known by
# several names: Bradley-Terry, Zermelo, and KRACH. I have chosen to call it BTZ after its three
# co-discoverers: Bradley and Terry, who worked together, and Zermelo, who worked independently.
# I have, however, extended the rating system to allow for more advanced comparisons.
# 
# Basic BTZ ratings have the following property:
# _ni_   Ki
# \    ------- = Vi
# /___ Ki + Kj
# j=1
# 
# Where:
#  ni = Number of comparisons involving entity i
#  j  = Opponent of entity i
#  Ki = Rating of entity i
#  Kj = Rating of entity j
#  Vi = Comparisons won by entity i
# 
# This makes them extremely useful for predicts, as the fraction Ki / (Ki + Kj) represents the
# probability that entity i will beat entity j in a comparison.
# 
# To compute KRACH ratings, the following formula is used:
#           Vi
#      ------------
# Ki =  _ni_   Nij
#       \    -------
#       /___ Ki + Kj
#       j=1
#
# Where all the variables are the same as above except:
#  Nij = Number of comparisons between entity i and entity j
#
# Because a team's rating depends on itself, the ratings are calculated iteratively until they
# converge.
#
# This concept has been extended in order to simulate matchups more accurately by taking each
# team's individual strengths and weaknesses into account.
# 
#
# EXTENSION #1: VICTORY POINTS
# ----------------------------
# The BTZ concept can be extended to be aware of margin of victory instead of simple win/loss.
# To do this, the winning margin is mapped to "victory points" between 0 and 1 for each comparison
# using a logistic function.
#          1
# Vp = -----------
#      1 + e^(-k * (m - m0))
#
# Where:
#  Vp = Number of victory points awarded.
#  k  = Constant chosen to scale the points.
#  m0 = Constant chosen to shift the points.
#  m  = Margin of victory for the entity being awarded points (is negative for the loser).
#
# Theoretically speaking, the constants should be chosen to maximize the correlation between
# predicted wins and actual wins. Practically speaking, this isn't usually efficient to compute
# this directly and numerically due to other adjustments that may be present. So instead, the
# constants should be chosen to maximize the correlation between the metric being converted to
# victory points and some binary variable measuring actual wins. For indirect metrics, such as
# goals scored or shots allowed, this can simply be whether the given team actually won the game.
# For a metric like scoring margin, where the outcome of the comparison can be directly inferred
# from the metric itself, the next comparison between the two entities might be used as a proxy.
# To find the values, then, a logistic regression is computed between the metric and the win proxy.
# 
# 
# EXTENSION #2: ASYMMETRIC COMPARISONS
# ------------------------------------
# The BTZ concept can be extended to be aware of offense/defense-type divisions, for example, the
# actual score of a game. In this case, each team gets two BTZ ratings: one for its offense and one
# for its defense. To calculate the ratings, the following formulas are used:
#             Vsi                         Vsj
#       ----------------            ----------------
# Kio =  _ni_   Nij         Kid =   _ni_   Nij
#        \    ---------             \    ---------
#        /___ Kio + Kjd             /___ Kid + Kjo
#        j=1                        j=1
#
# Where:
#  Kxo = Offensive rating for entity i / j if x = i or j, respectively
#  Kxd = Defensive rating for entity i / j if x = i or j, respectively
#  Vsx = Victory points awarded to entity i / j based on the points they scored in the comparison
#        if x = i or j, respectively
#
#
# EXTENSION #3: ADJUSTMENT FACTORS
# --------------------------------
# The BTZ concept can be extended to allow for adjustment factors that may affect an entity's
# rating. For example: home/away games, attendance, travel time, etc. The factors are applied to
# the entity's rating when computing the rating or probability of victory. A factor like home/away
# can simply be a conditionally applied adjustment, while a numerical factor like attendance can be
# fed into any arbitrary function before applying it to the rating. The functions normalizing the
# adjustment factors can be standardized across whatever population seems best: the entire field
# of entities, a conference, or a single entity.
# The probability of victory can be computed as follows:
#         Ki
# ------------------- = Vi
# Ai*Bi*Ki + Aj*Bj*Kj
# 
# Where:
#  (A|B|...)(i|j) = Adjustment factor for entity i
#
# 
# CODE NOTES
# ----------
# This code is designed to compute BTZ ratings as generically as possible. YAML files describe the
# parameters that go into a set of ratings, including name, data sources, asymmetry, and any
# adjustment factors. Then, by providing the initializer with the name of the BTZ ratings, the
# class will look up the corresponding YAML configuration and compute the ratings.
#
#
# APPLICATION NOTES
# -----------------
# The ultimate goal is to break down a hockey game into its most specific constituent events, and
# then build a prediction system off of that. The current plan is as follows:
# 1) Predict shot attempts for each team using asymmetric BTZ ratings for Shot Attempts For and Shot Attempts Allowed.
# 2) Predict % of shot attempts that become SOG for each team using asymmetric BTZ ratings for SOG For and SOG Allowed.
# 3) Predict % of SOG that become goals for each team using asymmetric BTZ ratings for Save % For and Save % Allowed.
# 4) Multiply the above predictions together to get predicted goals for each team.
# 5) Use the predicted goals to generate a Poisson Distribution for each team.
#
# Each of the above can be further broken down by situation: Period, PP/PK, Empty Net, etc. to give
# a more accurate view of the probability.
# Then partial game probabilities can be generated for games currently underway based on situation.
# 

require 'open3'
require 'yaml'
require 'securerandom'
require_relative 'btz_group_calculator'
require_relative 'btz_utils'
rake_require 'utils'

# Load a configuration file from a filename.
# @param fn [String] The path to the file.
# @return [Hash] The configuration.
def load_cfg(fn)
  YAML.load(File.read(fn))
end

# Create a task that uses a configuration file.
# @param args [Array] The arguments to create the task.
# @yield [Task, Hash, TaskArguments] The task, configuration hash, and invocation arguments.
def cfg_task(*args, &block)
  parse_task_args(*args) do |name, targs, prereqs|
    task name, ([:cfg] + targs) => prereqs do |t, iargs|
      if block_given?
        cfg = load_cfg(iargs[:cfg])
        yield t, cfg, iargs
      end
      puts "BTZ #{name}[#{iargs.to_a.join(',')}]"
    end
  end
end

# Create a task that accesses the database and takes a configuration filename parameter.
# @param args [Array] The arguments to create the task.
# @yield [Task, MySql, Hash, TaskArguments] The task, database connection, configuration hash, and invocation arguments.
def cfg_db_task(*args, &block)
  parse_task_args(*args) do |name, targs, prereqs|
    access_db name, ([:cfg] + targs) => prereqs do |t, db, iargs|
      if block_given?
        cfg = load_cfg(iargs[:cfg])
        yield t, db, cfg, iargs
      end
      puts "DB #{name}[#{iargs.to_a.join(',')}]"
    end
  end
end

# Create a task that accesses the database and takes a configuration filename parameter.
# @param args [Array] The arguments to create the task.
# @yield [Task, MySql, Hash, Integer, TaskArguments] The task, database connection, configuration hash, group ID, and invocation arguments.
def cfg_grp_multitask(*args, &block)
  cfg_db_task(*args) do |t, db, cfg, args|
    immediate_multitask "#{task_id(t, args)}_grps" => (db.query("SELECT DISTINCT grp FROM `#{cfg[:ratings_table]}`").map(&:first).map do |grp|
      task "#{task_id(t, args)}_#{grp}" do
        yield t, cfg, grp, args
      end
    end)
  end
end

# Get a unique task ID for the task with its parameters
# @param t [Task] The task.
# @param args [TaskArguments] The task arguments.
def task_id(t, args)
  "#{t.name.to_s}[#{args.to_a.map(&:to_s).join(',')}]"
end

desc "Namespace for BTZ-related tasks."
namespace :btz do

  desc "Setup the database for this BTZ system."
  cfg_db_task :setup_db do |t, db, cfg, args|
    db.query(BtzUtils.erb_from_cfg(File.join(*%w[btz setup_db.sql.erb]), cfg))
  end

  desc "Calculate the grouping of the entities in the BTZ system."
  cfg_task :update_groups => %i[update_data setup_db] do |t, cfg, args|
    BtzGroupCalculator.new(cfg).calculate()
  end

  desc "Calculate the VP coefficients for the BTZ system."
  task :update_vp_coeffs, [:cfg] => %i[update_groups] do |t, args|
    `pipenv run python lib/py/btz_vp_coeffs_calculator.py #{args[:cfg]}`
    puts "DB #{task_id(t, args)}"
  end

  desc "Calculate the adjustment coefficients for the BTZ system."
  task :update_adj_coeffs, [:cfg] => %i[update_vp_coeffs] do |t, args|
    `pipenv run python lib/py/btz_adj_coeffs_calculator.py #{args[:cfg]}`
    puts "DB #{task_id(t, args)}"
  end

  desc "Calculate the ratings for the BTZ system."
  task :update_ratings, [:cfg] => %i[update_adj_coeffs] do |t, args|
    `pipenv run python lib/py/btz_ratings_calculator.py #{args[:cfg]}`
    puts "DB #{task_id(t, args)}"
  end

  desc "Clean up any temporary tables/views created by BTZ"
  access_db :cleanup do |t, db|
    db.query("SHOW FULL TABLES;").each do |table, type|
      if table.match(/[0-9a-fA-F]{32}/) or table.match(/_\d+$/)
        if type == 'VIEW'
          db.query("DROP VIEW `#{table}`;")
        else
          db.query("DROP TABLE `#{table}`;")
        end
      end
    end
  end
end






# OLD



# Gets the value of a Poisson pmf at the designated value.
# @param rate [Float] The event rate (lambda)
# @param k [Integer] The value whose probability should be calculated.
def poisson_at_value(rate, k)
  (((rate ** k) * Math.exp(-rate)) / (1..k).reduce(1, :*))
end

# Gets the discrete probabilities for a Poisson pmf up to a certain point.
# @param rate [Float] The event rate (lambda)
# @param leftover [Float] The amount of probability to be left over at the end.
def poisson_pmf(rate, leftover)
  cum_prob = 0
  pmf = []
  k = 0
  while (1 - cum_prob) > leftover
    prob = poisson_at_value(rate, k)
    pmf << prob
    cum_prob += prob
    k += 1
  end
  pmf
end

UPDATE_BTZ_REGEX = /^update_btz:(\w*)$/
db_rule(UPDATE_BTZ_REGEX, %i[grp]) do |t, db, args|
  BTZ.new(t.name.match(UPDATE_BTZ_REGEX)[1]).update(args[:grp] ? args[:grp].to_i : args[:grp])
end

PREDICT_REGEX = /^btz_predict:(\w*)$/
db_rule(PREDICT_REGEX, %i[t1 t2]) do |t, db, args|
  puts BTZ.new(t.name.match(PREDICT_REGEX)[1]).predict(args[:t1], args[:t2])
end

PREDICT_POISSON_REGEX = /^predict_poisson:(\w*)$/
db_rule(PREDICT_POISSON_REGEX, %i[t1 t2]) do |t, db, args|
  fr, against = BTZ.new(t.name.match(PREDICT_POISSON_REGEX)[1]).predict(args[:t1], args[:t2])

  printf("Value For       Probability\n")
  printf("---------------------------\n")
  poisson_pmf(fr, 0.00001).each_with_index do |prob, value|
    printf("%-16s %s\n", value, prob)
  end

  printf("\n")
  printf("Value Against   Probability\n")
  printf("---------------------------\n")
  poisson_pmf(against, 0.00001).each_with_index do |prob, value|
    printf("%-16s %s\n", value, prob)
  end
end

POISSON_COMPARE_REGEX = /^poisson_compare:(\w*)$/
db_rule(POISSON_COMPARE_REGEX, %i[t1 t2]) do |t, db, args|
  fr, against = BTZ.new(t.name.match(POISSON_COMPARE_REGEX)[1]).predict(args[:t1], args[:t2])

  mov_prob = {}
  pg0 = 0
  pl0 = 0
  pe0 = 0
  poisson_pmf(fr, 0.00001).each_with_index do |pf, vf|
    poisson_pmf(against, 0.00001).each_with_index do |pa, va|
      prob = pf * pa
      mov = vf - va
      mov_prob[mov] ||= 0
      mov_prob[mov] += prob
      pg0 += prob if mov > 0
      pl0 += prob if mov < 0
      pe0 += prob if mov == 0
    end
  end

  pp pg0, pe0, pl0, mov_prob
end
