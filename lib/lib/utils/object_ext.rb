# Re-open object for editing
class Object

  public

    # Prepares an object to be inserted into a SQL query.
    # @return [String] The appropriate text representation of the object.
    def to_sql
      if nil?
        'NULL'
      elsif is_a?(Numeric)
        to_s
      elsif is_a?(TrueClass) || is_a?(FalseClass)
        to_s.upcase
      else
        "'#{to_s.gsub(/'/, '\'\'')}'"
      end
    end

    # Equivalent to tap { |o| puts o }
    def spy
      puts self
      self
    end

end
