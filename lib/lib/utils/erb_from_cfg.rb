require_relative 'setup_env'

# Needed libs
require 'erb'
require 'yaml'

# Require these for use in the template
require 'btz_utils'
require 'utils'
require 'ext'

# Render template to stdout
puts BtzUtils.erb_from_cfg(ARGV[0], YAML.load(File.read(ARGV[1])))
