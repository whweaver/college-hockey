require 'utils'

# Re-open the module enumerable for editing
module Enumerable

  public

    # Executes the block for each object in its own thread and joins them at
    # the end.
    # @yield [Object] Each object in the enumerable.
    def each_thread(&block)
      threads = []
      each { |o| threads << Thread.new { yield o } }
      threads.each { |t| t.join }
    end

    # Executes the block for each object in its own thread and joins them at
    # the end. Opens a DB connection for each object as well.
    # @yield [Mysql] A database connection.
    # @yield [Object] Each object in the enumerable.
    def each_db_thread(&block)
      each_thread { |o| db_update { |db| yield db, o } }
    end

end