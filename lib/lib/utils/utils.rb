require 'nokogiri'
require 'mysql'
require 'net/http'
require 'yaml'

require_relative 'ext'

db_cfg = nil

# Aliases for CHN time zones to Ruby time zones
TIME_ZONE_ALIASES = {
  'ET' => 'Eastern Time (US & Canada)',
  'CT' => 'Central Time (US & Canada)',
  'MT' => 'Mountain Time (US & Canada)',
  'PT' => 'Pacific Time (US & Canada)',
  'AT' => 'Alaska'
}

# Create a Nokogiri HTML object from a file
# @param filepath [String] The path to the file
# @return [Nokogiri::HTML] A Nokogiri HTML object
def nokogiri_html(filepath)
  Nokogiri::HTML( $hard_drive_mutex.synchronize { File.read(filepath, encoding: 'utf-8') })
end

# Determines the season cutoff date for the current year.
# @param year [Integer] The current year.
# @return [DateTime] The season cutoff.
def season_cutoff(year)
  Time.new(year, 9, 1)
end

# Determines the current season.
# @return [String] The current season in the form '20152016'
def cur_season
  cur_date = Time.now
  cur_year = cur_date.year
  if cur_date > season_cutoff(cur_year)
    "#{cur_year}#{cur_year + 1}"
  else
    "#{cur_year - 1}#{cur_year}"
  end
end

# Connect to the hockey database as the updater and yield the connection to a block before closing it.
# @param block [Block] The block to yield the connection object to.
# @return [Object] The return value of the block.
def db_update(&block)
  db_cfg ||= YAML.load(File.read('lib/yml/db.yml'))
  m = Mysql.new(db_cfg[:hostname], db_cfg[:username], db_cfg[:password], db_cfg[:database], db_cfg[:port], db_cfg[:socket])
  m.charset = 'utf8'
  rv = nil
  begin
    rv = yield m if block_given?
  ensure
    m.close
  end
  rv
end

# Split a name into first and last name.
# @param full_name [String] The full name in either "first last" or "last, first" format.
# @param last_name_first [Boolean] Whether or not the last name comes first.
# @return [Array] The split name as [first, last]
def split_name(full_name, last_name_first = false)
  if last_name_first
    full_name.split(/,\s+/, 2).reverse
  else
    full_name.rsplit(/\s+/, 2)
  end
end

# Resolve a path with redirection.
# @param path [String] The potentially redirected path.
# @return [String] The actual path.
def resolve_path(path)
  response = Net::HTTP.get_response(URI("https://www.collegehockeynews.com/#{path}"))
  case response.code
    when '200'
      path
    when '301'
      response['location']
    else
      raise "Unknown response code #{response.code}!"
  end
end

# Write a database table to CSV.
# @param db [Mysql] The database connection to use.
# @param table [String] The table to write.
def table_to_csv(db, table)
  results = db.select(table)
  FileUtils.mkdir_p('stats')
  File.open(File.join('stats', table.ext('csv')), 'w') do |f|
    f << results.fetch_fields.map(&:name).join(',') + "\n"
    results.each { |result| f << result.join(',') + "\n" }
  end
end

# Evaluate an ERB using standard parameters and error handling.
# @param erb [String] Path to the ERB to evaluate.
# @param b [Binding] Binding to use for the ERB.
# @return [String] The result of the ERB.
def standard_erb(erb, b=binding)
  begin
    ERB.new(File.read(erb), nil, "<>").result(b)
  rescue StandardError => e
    puts "Failed ERB was '#{erb}'"
    raise e
  end
end

# Interfaces with minimizer.py to minimize any given function.
class Minimizer

  # Start a minimizing thread
  def initialize
    @stdin, @stdout, @stderr, @wait_thr = Open3.popen3("pipenv run python py/minimizer.py")
  end

  # Send a command to the python minimizer.
  # @cmd [Array] The command tokens to send to the minimizer.
  def put_command(cmd)
    @stdin.puts(cmd.join(' '))
  end

  # Minimize a function.
  # @param x0 [Array] The initial parameters to pass to func.
  # @yield [Float, Float, ...] The function to minimize.
  # @return [Array] The parameters resulting in the minimum value of func.
  def minimize(*x0, &block)
    min_vals = nil

    put_command(['minimize'] + x0)

    loop do
      command = nil
      begin
        command = @stdout.readline.split
      rescue
        raise @stderr.read
      end
      command_name = command[0]

      case command_name
      when 'call'
        rv = yield *command[1..-1].map(&:to_f)
        put_command(['return', rv])

      when 'return'
        min_vals = command[1..-1].map(&:to_f)
        break

      else
        raise "Unknown command received: #{command}"
      end
    end

    min_vals
  end

  # Close the thread
  def close
    @stdin.puts('kill') if @wait_thr.status
    raise @stderr.read if @wait_thr.status.nil?
    @stdin.close
    @stdout.close
    @stderr.close
  end
end
