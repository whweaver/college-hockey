require_relative 'object_ext'
require_relative 'string_ext'
require_relative 'mysql_ext'
require_relative 'enumerable_ext'

# Mutex to prevent stdout jumbling
$stdout_mutex = Mutex.new

# Ensure output to stdout is handled properly with multitasks.
# @param args [Array] The arguments to puts.
def puts(*args)
  $stdout_mutex.synchronize { super(*args) }
end
