require 'open-uri'
require 'net/http'
require 'concurrent'
require 'rake'
require 'yaml'

require 'ext'
require 'utils'

# Re-open Concurrent module for editing
module Concurrent

  # Re-open Semaphore class for editing
  class Semaphore

    # Shorthand for acquiring/releasing the same number of permits.
    # @param permits [Integer] The number of permits to acquire.
    # @param block [Block] The block to execute.
    # @return The value returned by the block.
    def synchronize(permits=1, &block)
      acquire(permits)
      rv = yield if block_given?
      release(permits)
      rv
    end
  end
end

# Semaphore to make network accesses faster
$network_semaphore = Concurrent::Semaphore.new(16)

# Mutex to allow faster hard drive access
$hard_drive_mutex = Mutex.new

# Expose the rake_require method directly.
# @param args [Array] The args to pass to rake_require.
def rake_require(*args)
  Rake.application.rake_require(*args)
end

# Wrapper for rake require that searches for a file relative to the current directory.
# @param filepath [String] The path to the file to require.
def rake_require_relative(filepath)
  rake_require filepath, [File.dirname(caller_locations.first.absolute_path)]
end

# Parses the arguments from a task declaration.
# @param args [Array] The arguments passed to the task declaration block.
# @yield [String, Array, Array] The name, parameters, and prerequisites for the task.
# @return [Object] The return value of the block, if given, otherwise a hash containing the name,
#   args, and prereqs for the task.
def parse_task_args(*args, &block)
  name = nil
  iargs = []
  prereqs = []
  if args[0].is_a?(Hash)
    name = args[0].keys[0]
    prereqs = [args[0][name]].flatten
  else
    name = args[0]
    if args[1].is_a?(Hash)
      iargs = [args[1].keys[0]].flatten
      prereqs = [args[1][iargs]].flatten
    elsif args[1]
      iargs = [args[1]].flatten
    end
  end
  if block_given?
    yield name, iargs, prereqs
  else
    { name: name, args: iargs, prereqs: prereqs }
  end
end

# Create a FileTask to download a file and place it in the given destination.
# @param hsh [Hash] Task-style hash of filename and dependencies, including URL.
def download(hsh)
  dest = hsh.keys[0]
  url = nil
  deps = []
  hsh.values.flatten.each do |dep|
    if dep.respond_to?(:include?) and dep.include?('://')
      if url.nil?
        url = dep
      else
        raise 'More than one URL provided to download!'
      end
    else
      deps << dep
    end
  end
  file dest => deps do
    $hard_drive_mutex.synchronize { FileUtils.mkdir_p(File.dirname(dest)) }

    # Retry the download up to 10 times
    retries = 0
    while retries < 10
      begin
        url_contents = $network_semaphore.synchronize { open(URI.escape(url)) { |w| w.read } }
        break
      rescue Net::OpenTimeout => e
        retries += 1
      end
    end
    
    $hard_drive_mutex.synchronize { File.write(dest, url_contents) }
    puts "DOWNLOAD #{dest}"
  end
end

# Create a FileTask to extract data from an HTML file and insert it into the database. Creates a dummy file for timestamp tracking of the operation.
# @param hsh [Hash] Task-style hash of dummy file and HTML dependencies.
# @param block [Block] The block to execute. An open MySQL connection and Nokogiri HTML objects for each dependency are passed in as arguments.
def html_to_db(hsh, &block)
  prereqs = hsh.values.flatten
  dest = hsh.keys[0]
  file hsh do
    db_update { |m| yield m, *prereqs.map { |pr| nokogiri_html(pr) } } if block_given?
    FileUtils.mkdir_p(File.dirname(dest))
    FileUtils.touch(dest)
    puts "DB #{dest}"
  end
end

# Create a task to access the database.
# @param args [Array] Task arguments.
# @param block [Block] The block the execute in the task. An open MySQL connection is passed in.
def access_db(*args, &block)
  multitask *args do |t, targs|
    db_update { |m| yield t, m, targs } if block_given?
  end
end

# Create a rule to access the database.
# @param args [Array] Arguments for the rule.
# @param block [Block] The block the execute in the task. An open MySQL connection is passed in.
def db_rule(*args, &block)
  rule *args do |t, targs|
    db_update { |m| yield t, m, targs } if block_given?
  end
end

# Create a rule to access the database and generate a dummy file.
# @param args [Array] Arguments for the rule.
# @param block [Block] The block to execute in the task. An open MySQL connectoin is passed in.
def db_rule_with_dummy(*args, &block)
  db_rule *args do |t, db, args|
    db_path = t.name.db_dummy_path
    yield t, db, args if block_given?
    FileUtils.mkdir_p(File.dirname(db_path))
    FileUtils.touch(db_path)
    puts "DB #{db_path}"
  end
end

# Create a task to access the database and generate a dummy file.
# @param args [Array] Arguments for the task.
# @param block [Block] The block to execute in the task. An open MySQL connectoin is passed in.
def db_task_with_dummy(*args, &block)
  access_db *args do |t, db, args|
    db_path = t.name.db_dummy_path
    yield t, db, args if block_given?
    FileUtils.mkdir_p(File.dirname(db_path))
    FileUtils.touch(db_path)
    puts "DB #{db_path}"
  end
end

# Create db_rule_with_dummy to run a query from a SQL file.
# @param args [Array] Arguments for the rule.
def db_query_rule(*args)
  db_rule_with_dummy *args do |t, db|
    db.query(File.read(t.prerequisites[0]))
  end
end

# Create a rule to execute an ERB.
# @param args [Array] Arguments for the rule.
# @param block [Block] The block should return the binding to use in the ERB.
def erb_rule(*args, &block)
  rule *args do |t, args|
    b = yield t, args if block_given?
    b ||= binding
    begin
      FileUtils.mkdir_p(File.dirname(t.name))
      File.write(t.name, ERB.new(File.read(t.prerequisites[0].ext('erb')), nil, "<>").result(b))
      puts "ERB #{t.name}"
    rescue StandardError => e
      puts "Failed ERB was '#{t.name}'"
      raise e
    end
  end
end

# Create a rule to execute an ERB with a configuration file in the binding.
# @param args [Array] Arguments for the rule.
def cfg_erb_rule(*args)
  erb_rule *args do |t, args|
    cfg = YAML.load(File.read(t.prerequisites[1]))
    binding
  end
end

# Create a task to setup a new season at the season cutoff.
# @param paths [Array] The paths to remove at the new season.
def setup_season(*paths)
  task :setup_season do
    paths.each do |path|
      cur_time = Time.now
      cutoff = season_cutoff(cur_time.year)
      abs_path = path.to_absolute
      FileUtils.rm_rf(abs_path) if File.exist?(abs_path) and (cur_time > cutoff) and (File.mtime(abs_path) < cutoff)
    end
  end
end

# Create tasks to download and parse an HTML page to the DB.
# @param hsh [Hash] The task-style hash.
# @param block [Block] The block to execute for the HTML parser.
def download_db(hsh, &block)
  partial_path = hsh.keys[0]
  html_path = partial_path.html_path
  db_path = partial_path.db_dummy_path
  download html_path => hsh.values[0]
  html_to_db(db_path => html_path, &block)
end

# Create a multitask and run it immediately.
# @param args [Array] The task arguments.
# @param block [Block] The block to execute.
def immediate_multitask(*args, &block)
  multitask(*args, &block).invoke
end

def db_multitask(hsh, query, &block)
  access_db hsh do |t, db|
    immediate_multitask "#{hsh.keys.first}_tasks" => (db.query(query).map(&block).compact)
  end
end
