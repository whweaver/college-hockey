# Re-open string for editing
class String

  private

    alias_method :orig_strip, :strip
    alias_method :orig_match, :match

  public

    # Interpret the string as a relative filepath and convert it to an absolute filepath.
    # @return [String] The absolute filepath.
    def to_absolute
      $hard_drive_mutex.synchronize { File.join(Dir.pwd, self) }
    end

    # Replace weird whitespace with spaces and strip it.
    # @return The stripped and converted string
    def strip
      gsub(/\u00A0/, ' ').orig_strip
    end

    # Split, counting from the end, not the beginning, for limits.
    # @param args [Array] The args to pass to split.
    # @return [Array] The split string.
    def rsplit(*args)
      reverse.split(*args).map(&:reverse).reverse
    end

    # Indirect #match through another method call. This somehow fixes the thread-safety issue with
    # #match in which it can get confused between two match calls happening simultaneously.
    # @param args [Array] The arguments to #match.
    # @return [Object] The return value of match.
    def match(*args, &block)
      orig_match(*args, &block)
    end

    # Expand data path to DB dummy path.
    # @return [String] The path to the DB dummy.
    def db_dummy_path
      File.join(*%w[tmp db_management db_dummies], self)
    end

    # Expand data path to HTML path.
    # @return [String] The path to the HTML.
    def html_path
      File.join(*%w[tmp db_management html], self)
    end
end
