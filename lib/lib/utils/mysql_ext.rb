require 'erb'
require 'mysql'

# Re-open Mysql for editing
class Mysql

  private

    alias_method :orig_query, :query

    # Starts a write lock on the given tables and disables autocommit.
    # @param tables [Array] The tables to lock in the format {table: :read/:write}
    def start_lock(tables)
      @locked = true
      query('SET autocommit = 0')
      q = "LOCK TABLES " + tables.map { |t, rw| "`#{t}` #{rw.upcase}" }.join(', ')
      query(q)
    end

    # Unlocks all tables.
    def unlock
      query('UNLOCK TABLES')
      query('SET autocommit = 1')
      @locked = false
    end

    # Rolls back all changes and unlocks all tables
    def rollback_unlock
      query('ROLLBACK')
      unlock
    end

  public

    # Wrapper around query to make debugging easier.
    # @param str [String] The query string.
    # @yield The result of the final query executed.
    # @return [ResultSet] The results of the query.
    def query(str, &block)
      rv = nil
      queries = str.split(';;').map(&:strip).reject(&:empty?)
      queries.each_with_index do |q, i|
        begin
          if i < queries.length - 1
            rv = orig_query(q)
          else
            rv = orig_query(q, &block)
          end
        rescue Mysql::Error, Mysql::ServerError => e
          puts "Failed query was:\n#{q}"
          raise e
        end
      end
      rv
    end

    # Executes the given block with a lock on the given tables. Can be nested,
    # but all nested locks must lock a subset of the tables locked in the
    # outermost lock. Otherwise, an exception will be raised. All queries will
    # be committed at the end of the block, or rolled back if an exception
    # occurs.
    # @param args [Array] The arguments.
    # @param block [Block] The block to execute.
    # @return [Object] The return value of the block.
    def lock(*args, &block)
      all_tables = []

      args.each do |a|
        if a.is_a?(Hash)
          a.each { |t, rw| all_tables << [t, rw.to_s.upcase] }
        else
          all_tables << [a, 'WRITE']
        end
      end

      # Lock
      if @locked
        rollback_unlock
        raise "Cannot lock tables #{tables.join(', ')} when already in a lock!"
      else
        start_lock(all_tables)
      end

      # Execute the block
      begin
        rv = (yield if block_given?)
      rescue
        rollback_unlock
        raise
      end

      # Unlock
      query('COMMIT')
      unlock

      rv
    end

    # Executes an insert query on the db.
    # @param table [String] The name of the table to insert into.
    # @param assns [Hash] The assignments to perform in the format { column: value }
    # @return [ResultSet] The results of the query.
    def insert(table, assns)
      columns = assns.keys.map { |c| "`#{c}`" }.join(', ')
      values = assns.values.map { |v| v.to_sql }.join(', ')
      q = "INSERT INTO `#{table}` (#{columns}) VALUES (#{values})"
      query(q)
    end

    # Executes an update query on the db.
    # @param table [String] The name of the table to update.
    # @param assns [Hash] The assignments to perform in the format { column: value }
    # @param wheres [Hash] The values to check in the format { column: value }
    # @return [ResultSet] The results of the query.
    def update(table, assns, wheres)
      txt_assns = assns.map { |c, v| "`#{c}` = #{v.to_sql}" }.join(', ')
      txt_wheres = wheres.map { |c, v| "(`#{c}` = #{v.to_sql})"}.join(' AND ')
      query("UPDATE `#{table}` SET #{txt_assns} WHERE #{txt_wheres}")
    end

    # Executes a select query on the db.
    # @param table [String] The name of the table to select from.
    # @param cols [Array] The columns to select.
    # @param wheres [Hash] The values to check in the format { column: value }
    # @return [ResultSet] The results of the query.
    def select(table, cols=%w[*], wheres={})
      txt_cols = cols.map { |c| c == '*' ? c : "`#{c}`" }.join(', ')
      txt_wheres = wheres.map do |c, v|
        if v.nil?
          "(`#{c}` IS NULL)"
        else
          "(`#{c}` = #{v.to_sql})"
        end
      end.join(' AND ')
      q = "SELECT #{txt_cols} FROM `#{table}`"
      q << " WHERE #{txt_wheres}" unless wheres.empty?
      query(q)
    end

    # Executes an update query if the given entries are present, otherwise inserts them.
    # @param table [String] The name of the table to update.
    # @param assns [Hash] The assignments to perform in the format { column: value }
    # @param wheres [Hash] The values to check in the format { column: value }
    # @param id_col_names [Array] The names of the primary key columns.
    # @return [Result] The result of the select query with the wheres.
    def insert_or_update(table, assns, wheres, id_col_names=%w[*])
      if select(table, id_col_names, wheres).size == 0
        insert(table, assns)
      else
        update(table, assns, wheres)
      end
      select(table, id_col_names, wheres).fetch
    end

    # Executes an insert query if the given entry is not present.
    # @param table [String] The name of the table to update.
    # @param id_col_names [Array] The names of the primary key columns.
    # @param wheres [Hash] The values to check in the format { column: value }
    # @param wheres [Hash] The values to insert in the format { column: value }
    # @return [Object] The of the relevant row.
    def insert_if_not_exists(table, id_col_names, wheres, assns=nil)
      assns = wheres if assns.nil?
      results = select(table, id_col_names, wheres)
      raise "Multiple matches found!" if results.size > 1
      if results.size == 0
        insert(table, assns)
        if id_col_names.size == 1
          query('SELECT LAST_INSERT_ID()').fetch[0]
        else
          select(table, id_col_names, wheres).fetch
        end
      else
        if id_col_names.size == 1
          results.fetch[0]
        else
          results.fetch
        end
      end
    end

    # Saves a complete table to a CSV file.
    # @param table The table to save.
    # @param filepath The path to the CSV file to save to.
    def table_to_csv(table, filepath)
      results = select(table)
      FileUtils.mkdir_p(File.dirname(filepath))
      File.open(filepath, 'w') do |f|
        f << results.fetch_fields.map(&:name).join(',') + "\n"
        results.each { |result| f << result.join(',') + "\n" }
      end
    end

    # Runs a query from an ERB file.
    # @param erb [String] Filepath to the ERB.
    # @param b [Binding] Binding to use with evaluation.
    # @return [ResultSet] The result of the query.
    def erb_query(erb, b=binding)
      filepath = File.join('erb', erb.ext('.sql.erb'))
      s = File.read(filepath)
      begin
        q = ERB.new(s, nil, "<>").result(b)
      rescue StandardError => e
        puts "Failed ERB was '#{erb}'"
        raise e
      end
      query(q)
    end

    class ResultBase

      # Map corrollary to #each_hash.
      # @param args [Array] The args to pass to #each_hash.
      # @yield [Hash] Record data as a hash.
      # @return [Array] The new array.
      def map_hash(*args, &block)
        new_array = []
        each_hash(*args) { |h| new_array << (yield h) }
        new_array
      end
    end
end
