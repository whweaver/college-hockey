raise 'Need to be on jruby!' if RUBY_PLATFORM != 'java'

require 'bundler'
begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  raise LoadError.new("Unable to Bundler.setup(): You probably need to run `bundle install`: #{e.message}")
end

# Setup require path
$LOAD_PATH.unshift(*Dir.glob('lib/lib/**/').map { |r| File.expand_path(r) })
